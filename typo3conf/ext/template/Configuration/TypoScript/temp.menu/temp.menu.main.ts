temp.menu.main = HMENU
temp.menu.main {
  entryLevel = 0
  1 = TMENU
  1 {
    wrap = <ul class="nav">|</ul>
    noBlur = 1
    NO = 1
    NO {
      wrapItemAndSub = <li>|</li>
      stdWrap.htmlSpecialChars = 1
    }
    ACT < .NO
    ACT {

      wrapItemAndSub = <li class="active">|</li>
      ATagBeforeWrap = 1
      stdWrap.wrap = <span>|</span>
    }
  }

  # 2. Ebene

  2 < .1
  2.wrap = <ul class="submenu">|</ul> 
  2.ACT {
      wrapItemAndSub = <li class="is-active">|</li>
      ATagBeforeWrap = 1
      stdWrap.wrap = <span>|</span>
      ATagParams = class="is-active"    
  }

}