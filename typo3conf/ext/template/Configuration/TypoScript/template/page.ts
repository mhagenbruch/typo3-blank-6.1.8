page = PAGE

page.10 = FLUIDTEMPLATE
page.10 {
	file = EXT:template/Resources/Private/Templates/index.html
}

page.config.index_enable = 1

plugin.tx_indexedsearch {
  show {
    rules = 0
    advancedSearchLink = 0
  }
  blind {
    results = 1
  }
}


page.10.variables {

	# Configuration
	baseUrl = TEXT
	baseUrl.value < config.baseURL


  # Backend Layout
  layout = TEXT
  layout.data = levelfield:-1,backend_layout_next_level,slide
  layout.override.field = backend_layout

  # Backend Layout
  language = TEXT
  language.value < config.language

  # Search URL:
  searchUrl = TEXT
  searchUrl.typolink {
    parameter = 20
    returnLast = url
  }

	# Menu
	mainMenu < temp.menu.main

  #Content
	content < styles.content.get

}
