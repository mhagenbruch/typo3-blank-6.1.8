lib.navigation.meta = COA
lib.navigation.meta {
	
	10 = HMENU
	10.special = directory
	10.special.value = 16
	10 {
		1 = TMENU
		1 {
			expAll = 1
			wrap = <ul class="nav navbar-nav">|</ul>
			
			NO = 1
			NO {
				wrapItemAndSub = <li>|</li>
				ATagTitle.field = subtitle // title
			}

			ACT < .NO
			ACT = 1
			ACT {
				wrapItemAndSub = <li class="active">|</li>
				#ATagParams = class="active"
			}
		}
	}
}