lib.navigation.main = COA
lib.navigation.main {
	
	10 = HMENU
	10 {
		1 = TMENU
		1 {
			expAll = 1
			wrap = <ul class="nav">|</ul>
			
			NO = 1
			NO {
				wrapItemAndSub = <li>|</li>
				ATagTitle.field = subtitle // title
			}

			ACT < .NO
			ACT = 1
			ACT {
				#wrapItemAndSub = <li class="active">|</li>
				ATagParams = class="active"
			}
		}
	}
}