page = PAGE
page {

	10 = FLUIDTEMPLATE
	10 {
		file = EXT:template/Resources/Private/Templates/index.html
		layoutRootPath = EXT:template/Resources/Private/Templates/Layouts/

		variables {
			template = TEXT
			template.data = levelfield:-1, backend_layout_next_level, slide
			template.override.field = backend_layout
		}
	}

	# CSS files to be included
	includeCSS {
	}


	# JS files to be included
	includeJSFooter {
	}

	# Add some good classes to the bodytag to make a styling of special pages easier
	bodyTagCObject = COA
	bodyTagCObject   {
		stdWrap.wrap = <body class="|">

		# Add page UID
		10 = TEXT
		10 {
			value = page-{field:uid}
			insertData = 1
			noTrimWrap = || |
		}

		# Add uid of the backend-layout
		20 = TEXT
		20 {
			wrap = template-|
			data = levelfield:-1, backend_layout_next_level, slide
			override.field = backend_layout
			noTrimWrap = || |
		}
	}

}