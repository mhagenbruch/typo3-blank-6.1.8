# Menu:
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.navigation/lib.navigation.meta.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.navigation/lib.navigation.main.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.navigation/lib.navigation.footer.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.navigation/lib.navigation.language.ts">

# Content:
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.content/lib.content.main.ts">
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/lib.content/lib.content.footer.ts">

# Configuration:
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/template/config.ts">

# Page-Settings:
<INCLUDE_TYPOSCRIPT: source="FILE: EXT:template/Resources/Private/TypoScript/template/page.ts">