<?php
class FluidCache_News_News_partial_List_Item_f94bc4f2bffe1e94db4c714baf0dc2344421c1a2 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '<!--
	=====================
		Partials/List/Item.html
-->
<div class="article articletype-';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments1 = array();
$arguments1['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.type', $renderingContext);
$arguments1['keepQuotes'] = false;
$arguments1['encoding'] = NULL;
$arguments1['doubleEncode'] = true;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
return NULL;
};
$value3 = ($arguments1['value'] !== NULL ? $arguments1['value'] : $renderChildrenClosure2());

$output0 .= (!is_string($value3) ? $value3 : htmlspecialchars($value3, ($arguments1['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments1['encoding'] !== NULL ? $arguments1['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments1['doubleEncode']));
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments4 = array();
// Rendering Boolean node
$arguments4['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.istopnews', $renderingContext));
$arguments4['then'] = ' topnews';
$arguments4['else'] = NULL;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper6 = $self->getViewHelper('$viewHelper6', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper6->setArguments($arguments4);
$viewHelper6->setRenderingContext($renderingContext);
$viewHelper6->setRenderChildrenClosure($renderChildrenClosure5);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper6->initializeArgumentsAndRender();

$output0 .= '">
	';
// Rendering ViewHelper Tx_News_ViewHelpers_ExcludeDisplayedNewsViewHelper
$arguments7 = array();
$arguments7['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper9 = $self->getViewHelper('$viewHelper9', $renderingContext, 'Tx_News_ViewHelpers_ExcludeDisplayedNewsViewHelper');
$viewHelper9->setArguments($arguments7);
$viewHelper9->setRenderingContext($renderingContext);
$viewHelper9->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper Tx_News_ViewHelpers_ExcludeDisplayedNewsViewHelper

$output0 .= $viewHelper9->initializeArgumentsAndRender();

$output0 .= '
	<!-- header -->
	<div class="header">
		<h3>
			';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments10 = array();
$arguments10['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments10['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments10['additionalAttributes'] = NULL;
$arguments10['uriOnly'] = false;
$arguments10['configuration'] = array (
);
$arguments10['class'] = NULL;
$arguments10['dir'] = NULL;
$arguments10['id'] = NULL;
$arguments10['lang'] = NULL;
$arguments10['style'] = NULL;
$arguments10['title'] = NULL;
$arguments10['accesskey'] = NULL;
$arguments10['tabindex'] = NULL;
$arguments10['onclick'] = NULL;
$arguments10['target'] = NULL;
$arguments10['rel'] = NULL;
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
$output12 = '';

$output12 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments13 = array();
$arguments13['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.title', $renderingContext);
$arguments13['keepQuotes'] = false;
$arguments13['encoding'] = NULL;
$arguments13['doubleEncode'] = true;
$renderChildrenClosure14 = function() use ($renderingContext, $self) {
return NULL;
};
$value15 = ($arguments13['value'] !== NULL ? $arguments13['value'] : $renderChildrenClosure14());

$output12 .= (!is_string($value15) ? $value15 : htmlspecialchars($value15, ($arguments13['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments13['encoding'] !== NULL ? $arguments13['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments13['doubleEncode']));

$output12 .= '
			';
return $output12;
};
$viewHelper16 = $self->getViewHelper('$viewHelper16', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper16->setArguments($arguments10);
$viewHelper16->setRenderingContext($renderingContext);
$viewHelper16->setRenderChildrenClosure($renderChildrenClosure11);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output0 .= $viewHelper16->initializeArgumentsAndRender();

$output0 .= '
		</h3>
	</div>

	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments17 = array();
// Rendering Boolean node
$arguments17['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.media', $renderingContext));
$arguments17['then'] = NULL;
$arguments17['else'] = NULL;
$renderChildrenClosure18 = function() use ($renderingContext, $self) {
$output19 = '';

$output19 .= '
		<!-- media preview element -->
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments20 = array();
$renderChildrenClosure21 = function() use ($renderingContext, $self) {
$output22 = '';

$output22 .= '
			<div class="news-img-wrap">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments23 = array();
// Rendering Boolean node
$arguments23['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews', $renderingContext));
$arguments23['then'] = NULL;
$arguments23['else'] = NULL;
$renderChildrenClosure24 = function() use ($renderingContext, $self) {
$output25 = '';

$output25 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments26 = array();
$renderChildrenClosure27 = function() use ($renderingContext, $self) {
$output28 = '';

$output28 .= '
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments29 = array();
$arguments29['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments29['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments29['additionalAttributes'] = NULL;
$arguments29['uriOnly'] = false;
$arguments29['configuration'] = array (
);
$arguments29['class'] = NULL;
$arguments29['dir'] = NULL;
$arguments29['id'] = NULL;
$arguments29['lang'] = NULL;
$arguments29['style'] = NULL;
$arguments29['title'] = NULL;
$arguments29['accesskey'] = NULL;
$arguments29['tabindex'] = NULL;
$arguments29['onclick'] = NULL;
$arguments29['target'] = NULL;
$arguments29['rel'] = NULL;
$renderChildrenClosure30 = function() use ($renderingContext, $self) {
$output31 = '';

$output31 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper
$arguments32 = array();
// Rendering Array
$array33 = array();
$array33['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews.0', $renderingContext);
$arguments32['map'] = $array33;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
$output35 = '';

$output35 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments36 = array();
// Rendering Boolean node
$arguments36['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 0);
$arguments36['then'] = NULL;
$arguments36['else'] = NULL;
$renderChildrenClosure37 = function() use ($renderingContext, $self) {
$output38 = '';

$output38 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments39 = array();
$output40 = '';

$output40 .= 'uploads/tx_news/';

$output40 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.image', $renderingContext);
$arguments39['src'] = $output40;
$arguments39['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.title', $renderingContext);
$arguments39['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.alt', $renderingContext);
$arguments39['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments39['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments39['additionalAttributes'] = NULL;
$arguments39['width'] = NULL;
$arguments39['height'] = NULL;
$arguments39['minWidth'] = NULL;
$arguments39['minHeight'] = NULL;
$arguments39['treatIdAsReference'] = false;
$arguments39['class'] = NULL;
$arguments39['dir'] = NULL;
$arguments39['id'] = NULL;
$arguments39['lang'] = NULL;
$arguments39['style'] = NULL;
$arguments39['accesskey'] = NULL;
$arguments39['tabindex'] = NULL;
$arguments39['onclick'] = NULL;
$arguments39['ismap'] = NULL;
$arguments39['longdesc'] = NULL;
$arguments39['usemap'] = NULL;
$renderChildrenClosure41 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper42 = $self->getViewHelper('$viewHelper42', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper42->setArguments($arguments39);
$viewHelper42->setRenderingContext($renderingContext);
$viewHelper42->setRenderChildrenClosure($renderChildrenClosure41);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output38 .= $viewHelper42->initializeArgumentsAndRender();

$output38 .= '
								';
return $output38;
};
$viewHelper43 = $self->getViewHelper('$viewHelper43', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper43->setArguments($arguments36);
$viewHelper43->setRenderingContext($renderingContext);
$viewHelper43->setRenderChildrenClosure($renderChildrenClosure37);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper43->initializeArgumentsAndRender();

$output35 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments44 = array();
// Rendering Boolean node
$arguments44['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 1);
$arguments44['then'] = NULL;
$arguments44['else'] = NULL;
$renderChildrenClosure45 = function() use ($renderingContext, $self) {
$output46 = '';

$output46 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments47 = array();
$arguments47['partial'] = 'Detail/MediaVideo';
// Rendering Array
$array48 = array();
$array48['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments47['arguments'] = $array48;
$arguments47['section'] = NULL;
$arguments47['optional'] = false;
$renderChildrenClosure49 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper50 = $self->getViewHelper('$viewHelper50', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper50->setArguments($arguments47);
$viewHelper50->setRenderingContext($renderingContext);
$viewHelper50->setRenderChildrenClosure($renderChildrenClosure49);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output46 .= $viewHelper50->initializeArgumentsAndRender();

$output46 .= '
								';
return $output46;
};
$viewHelper51 = $self->getViewHelper('$viewHelper51', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper51->setArguments($arguments44);
$viewHelper51->setRenderingContext($renderingContext);
$viewHelper51->setRenderChildrenClosure($renderChildrenClosure45);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper51->initializeArgumentsAndRender();

$output35 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments52 = array();
// Rendering Boolean node
$arguments52['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 2);
$arguments52['then'] = NULL;
$arguments52['else'] = NULL;
$renderChildrenClosure53 = function() use ($renderingContext, $self) {
$output54 = '';

$output54 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments55 = array();
$arguments55['partial'] = 'Detail/MediaHtml';
// Rendering Array
$array56 = array();
$array56['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments55['arguments'] = $array56;
$arguments55['section'] = NULL;
$arguments55['optional'] = false;
$renderChildrenClosure57 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper58 = $self->getViewHelper('$viewHelper58', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper58->setArguments($arguments55);
$viewHelper58->setRenderingContext($renderingContext);
$viewHelper58->setRenderChildrenClosure($renderChildrenClosure57);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output54 .= $viewHelper58->initializeArgumentsAndRender();

$output54 .= '
								';
return $output54;
};
$viewHelper59 = $self->getViewHelper('$viewHelper59', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper59->setArguments($arguments52);
$viewHelper59->setRenderingContext($renderingContext);
$viewHelper59->setRenderChildrenClosure($renderChildrenClosure53);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper59->initializeArgumentsAndRender();

$output35 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments60 = array();
// Rendering Boolean node
$arguments60['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 3);
$arguments60['then'] = NULL;
$arguments60['else'] = NULL;
$renderChildrenClosure61 = function() use ($renderingContext, $self) {
$output62 = '';

$output62 .= '
									';
// Rendering ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper
$arguments63 = array();
$arguments63['as'] = 'dam';
$arguments63['uid'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.uid', $renderingContext);
$renderChildrenClosure64 = function() use ($renderingContext, $self) {
$output65 = '';

$output65 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments66 = array();
$output67 = '';

$output67 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_path', $renderingContext);

$output67 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_name', $renderingContext);
$arguments66['src'] = $output67;
$arguments66['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.title', $renderingContext);
$arguments66['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.alt_text', $renderingContext);
$arguments66['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments66['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments66['additionalAttributes'] = NULL;
$arguments66['width'] = NULL;
$arguments66['height'] = NULL;
$arguments66['minWidth'] = NULL;
$arguments66['minHeight'] = NULL;
$arguments66['treatIdAsReference'] = false;
$arguments66['class'] = NULL;
$arguments66['dir'] = NULL;
$arguments66['id'] = NULL;
$arguments66['lang'] = NULL;
$arguments66['style'] = NULL;
$arguments66['accesskey'] = NULL;
$arguments66['tabindex'] = NULL;
$arguments66['onclick'] = NULL;
$arguments66['ismap'] = NULL;
$arguments66['longdesc'] = NULL;
$arguments66['usemap'] = NULL;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper69 = $self->getViewHelper('$viewHelper69', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper69->setArguments($arguments66);
$viewHelper69->setRenderingContext($renderingContext);
$viewHelper69->setRenderChildrenClosure($renderChildrenClosure68);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output65 .= $viewHelper69->initializeArgumentsAndRender();

$output65 .= '
									';
return $output65;
};
$viewHelper70 = $self->getViewHelper('$viewHelper70', $renderingContext, 'Tx_News_ViewHelpers_Format_DamViewHelper');
$viewHelper70->setArguments($arguments63);
$viewHelper70->setRenderingContext($renderingContext);
$viewHelper70->setRenderChildrenClosure($renderChildrenClosure64);
// End of ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper

$output62 .= $viewHelper70->initializeArgumentsAndRender();

$output62 .= '
								';
return $output62;
};
$viewHelper71 = $self->getViewHelper('$viewHelper71', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper71->setArguments($arguments60);
$viewHelper71->setRenderingContext($renderingContext);
$viewHelper71->setRenderChildrenClosure($renderChildrenClosure61);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output35 .= $viewHelper71->initializeArgumentsAndRender();

$output35 .= '
							';
return $output35;
};
$viewHelper72 = $self->getViewHelper('$viewHelper72', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper');
$viewHelper72->setArguments($arguments32);
$viewHelper72->setRenderingContext($renderingContext);
$viewHelper72->setRenderChildrenClosure($renderChildrenClosure34);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper

$output31 .= $viewHelper72->initializeArgumentsAndRender();

$output31 .= '
						';
return $output31;
};
$viewHelper73 = $self->getViewHelper('$viewHelper73', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper73->setArguments($arguments29);
$viewHelper73->setRenderingContext($renderingContext);
$viewHelper73->setRenderChildrenClosure($renderChildrenClosure30);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output28 .= $viewHelper73->initializeArgumentsAndRender();

$output28 .= '
					';
return $output28;
};
$viewHelper74 = $self->getViewHelper('$viewHelper74', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper74->setArguments($arguments26);
$viewHelper74->setRenderingContext($renderingContext);
$viewHelper74->setRenderChildrenClosure($renderChildrenClosure27);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output25 .= $viewHelper74->initializeArgumentsAndRender();

$output25 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments75 = array();
$renderChildrenClosure76 = function() use ($renderingContext, $self) {
$output77 = '';

$output77 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments78 = array();
// Rendering Boolean node
$arguments78['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments78['then'] = NULL;
$arguments78['else'] = NULL;
$renderChildrenClosure79 = function() use ($renderingContext, $self) {
$output80 = '';

$output80 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments81 = array();
$arguments81['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments81['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments81['additionalAttributes'] = NULL;
$arguments81['uriOnly'] = false;
$arguments81['configuration'] = array (
);
$arguments81['class'] = NULL;
$arguments81['dir'] = NULL;
$arguments81['id'] = NULL;
$arguments81['lang'] = NULL;
$arguments81['style'] = NULL;
$arguments81['title'] = NULL;
$arguments81['accesskey'] = NULL;
$arguments81['tabindex'] = NULL;
$arguments81['onclick'] = NULL;
$arguments81['target'] = NULL;
$arguments81['rel'] = NULL;
$renderChildrenClosure82 = function() use ($renderingContext, $self) {
$output83 = '';

$output83 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments84 = array();
$arguments84['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments84['title'] = '';
$arguments84['alt'] = '';
$arguments84['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments84['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments84['additionalAttributes'] = NULL;
$arguments84['width'] = NULL;
$arguments84['height'] = NULL;
$arguments84['minWidth'] = NULL;
$arguments84['minHeight'] = NULL;
$arguments84['treatIdAsReference'] = false;
$arguments84['class'] = NULL;
$arguments84['dir'] = NULL;
$arguments84['id'] = NULL;
$arguments84['lang'] = NULL;
$arguments84['style'] = NULL;
$arguments84['accesskey'] = NULL;
$arguments84['tabindex'] = NULL;
$arguments84['onclick'] = NULL;
$arguments84['ismap'] = NULL;
$arguments84['longdesc'] = NULL;
$arguments84['usemap'] = NULL;
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper86 = $self->getViewHelper('$viewHelper86', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper86->setArguments($arguments84);
$viewHelper86->setRenderingContext($renderingContext);
$viewHelper86->setRenderChildrenClosure($renderChildrenClosure85);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output83 .= $viewHelper86->initializeArgumentsAndRender();

$output83 .= '
								';
return $output83;
};
$viewHelper87 = $self->getViewHelper('$viewHelper87', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper87->setArguments($arguments81);
$viewHelper87->setRenderingContext($renderingContext);
$viewHelper87->setRenderChildrenClosure($renderChildrenClosure82);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output80 .= $viewHelper87->initializeArgumentsAndRender();

$output80 .= '
							</span>
						';
return $output80;
};
$viewHelper88 = $self->getViewHelper('$viewHelper88', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper88->setArguments($arguments78);
$viewHelper88->setRenderingContext($renderingContext);
$viewHelper88->setRenderChildrenClosure($renderChildrenClosure79);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output77 .= $viewHelper88->initializeArgumentsAndRender();

$output77 .= '
					';
return $output77;
};
$viewHelper89 = $self->getViewHelper('$viewHelper89', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper89->setArguments($arguments75);
$viewHelper89->setRenderingContext($renderingContext);
$viewHelper89->setRenderChildrenClosure($renderChildrenClosure76);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output25 .= $viewHelper89->initializeArgumentsAndRender();

$output25 .= '
				';
return $output25;
};
$arguments23['__thenClosure'] = function() use ($renderingContext, $self) {
$output90 = '';

$output90 .= '
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments91 = array();
$arguments91['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments91['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments91['additionalAttributes'] = NULL;
$arguments91['uriOnly'] = false;
$arguments91['configuration'] = array (
);
$arguments91['class'] = NULL;
$arguments91['dir'] = NULL;
$arguments91['id'] = NULL;
$arguments91['lang'] = NULL;
$arguments91['style'] = NULL;
$arguments91['title'] = NULL;
$arguments91['accesskey'] = NULL;
$arguments91['tabindex'] = NULL;
$arguments91['onclick'] = NULL;
$arguments91['target'] = NULL;
$arguments91['rel'] = NULL;
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
$output93 = '';

$output93 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper
$arguments94 = array();
// Rendering Array
$array95 = array();
$array95['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews.0', $renderingContext);
$arguments94['map'] = $array95;
$renderChildrenClosure96 = function() use ($renderingContext, $self) {
$output97 = '';

$output97 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments98 = array();
// Rendering Boolean node
$arguments98['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 0);
$arguments98['then'] = NULL;
$arguments98['else'] = NULL;
$renderChildrenClosure99 = function() use ($renderingContext, $self) {
$output100 = '';

$output100 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments101 = array();
$output102 = '';

$output102 .= 'uploads/tx_news/';

$output102 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.image', $renderingContext);
$arguments101['src'] = $output102;
$arguments101['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.title', $renderingContext);
$arguments101['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.alt', $renderingContext);
$arguments101['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments101['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments101['additionalAttributes'] = NULL;
$arguments101['width'] = NULL;
$arguments101['height'] = NULL;
$arguments101['minWidth'] = NULL;
$arguments101['minHeight'] = NULL;
$arguments101['treatIdAsReference'] = false;
$arguments101['class'] = NULL;
$arguments101['dir'] = NULL;
$arguments101['id'] = NULL;
$arguments101['lang'] = NULL;
$arguments101['style'] = NULL;
$arguments101['accesskey'] = NULL;
$arguments101['tabindex'] = NULL;
$arguments101['onclick'] = NULL;
$arguments101['ismap'] = NULL;
$arguments101['longdesc'] = NULL;
$arguments101['usemap'] = NULL;
$renderChildrenClosure103 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper104 = $self->getViewHelper('$viewHelper104', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper104->setArguments($arguments101);
$viewHelper104->setRenderingContext($renderingContext);
$viewHelper104->setRenderChildrenClosure($renderChildrenClosure103);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output100 .= $viewHelper104->initializeArgumentsAndRender();

$output100 .= '
								';
return $output100;
};
$viewHelper105 = $self->getViewHelper('$viewHelper105', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper105->setArguments($arguments98);
$viewHelper105->setRenderingContext($renderingContext);
$viewHelper105->setRenderChildrenClosure($renderChildrenClosure99);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output97 .= $viewHelper105->initializeArgumentsAndRender();

$output97 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments106 = array();
// Rendering Boolean node
$arguments106['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 1);
$arguments106['then'] = NULL;
$arguments106['else'] = NULL;
$renderChildrenClosure107 = function() use ($renderingContext, $self) {
$output108 = '';

$output108 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments109 = array();
$arguments109['partial'] = 'Detail/MediaVideo';
// Rendering Array
$array110 = array();
$array110['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments109['arguments'] = $array110;
$arguments109['section'] = NULL;
$arguments109['optional'] = false;
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper112 = $self->getViewHelper('$viewHelper112', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper112->setArguments($arguments109);
$viewHelper112->setRenderingContext($renderingContext);
$viewHelper112->setRenderChildrenClosure($renderChildrenClosure111);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output108 .= $viewHelper112->initializeArgumentsAndRender();

$output108 .= '
								';
return $output108;
};
$viewHelper113 = $self->getViewHelper('$viewHelper113', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper113->setArguments($arguments106);
$viewHelper113->setRenderingContext($renderingContext);
$viewHelper113->setRenderChildrenClosure($renderChildrenClosure107);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output97 .= $viewHelper113->initializeArgumentsAndRender();

$output97 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments114 = array();
// Rendering Boolean node
$arguments114['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 2);
$arguments114['then'] = NULL;
$arguments114['else'] = NULL;
$renderChildrenClosure115 = function() use ($renderingContext, $self) {
$output116 = '';

$output116 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments117 = array();
$arguments117['partial'] = 'Detail/MediaHtml';
// Rendering Array
$array118 = array();
$array118['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments117['arguments'] = $array118;
$arguments117['section'] = NULL;
$arguments117['optional'] = false;
$renderChildrenClosure119 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper120 = $self->getViewHelper('$viewHelper120', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper120->setArguments($arguments117);
$viewHelper120->setRenderingContext($renderingContext);
$viewHelper120->setRenderChildrenClosure($renderChildrenClosure119);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output116 .= $viewHelper120->initializeArgumentsAndRender();

$output116 .= '
								';
return $output116;
};
$viewHelper121 = $self->getViewHelper('$viewHelper121', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper121->setArguments($arguments114);
$viewHelper121->setRenderingContext($renderingContext);
$viewHelper121->setRenderChildrenClosure($renderChildrenClosure115);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output97 .= $viewHelper121->initializeArgumentsAndRender();

$output97 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments122 = array();
// Rendering Boolean node
$arguments122['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 3);
$arguments122['then'] = NULL;
$arguments122['else'] = NULL;
$renderChildrenClosure123 = function() use ($renderingContext, $self) {
$output124 = '';

$output124 .= '
									';
// Rendering ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper
$arguments125 = array();
$arguments125['as'] = 'dam';
$arguments125['uid'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.uid', $renderingContext);
$renderChildrenClosure126 = function() use ($renderingContext, $self) {
$output127 = '';

$output127 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments128 = array();
$output129 = '';

$output129 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_path', $renderingContext);

$output129 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_name', $renderingContext);
$arguments128['src'] = $output129;
$arguments128['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.title', $renderingContext);
$arguments128['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.alt_text', $renderingContext);
$arguments128['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments128['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments128['additionalAttributes'] = NULL;
$arguments128['width'] = NULL;
$arguments128['height'] = NULL;
$arguments128['minWidth'] = NULL;
$arguments128['minHeight'] = NULL;
$arguments128['treatIdAsReference'] = false;
$arguments128['class'] = NULL;
$arguments128['dir'] = NULL;
$arguments128['id'] = NULL;
$arguments128['lang'] = NULL;
$arguments128['style'] = NULL;
$arguments128['accesskey'] = NULL;
$arguments128['tabindex'] = NULL;
$arguments128['onclick'] = NULL;
$arguments128['ismap'] = NULL;
$arguments128['longdesc'] = NULL;
$arguments128['usemap'] = NULL;
$renderChildrenClosure130 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper131 = $self->getViewHelper('$viewHelper131', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper131->setArguments($arguments128);
$viewHelper131->setRenderingContext($renderingContext);
$viewHelper131->setRenderChildrenClosure($renderChildrenClosure130);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output127 .= $viewHelper131->initializeArgumentsAndRender();

$output127 .= '
									';
return $output127;
};
$viewHelper132 = $self->getViewHelper('$viewHelper132', $renderingContext, 'Tx_News_ViewHelpers_Format_DamViewHelper');
$viewHelper132->setArguments($arguments125);
$viewHelper132->setRenderingContext($renderingContext);
$viewHelper132->setRenderChildrenClosure($renderChildrenClosure126);
// End of ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper

$output124 .= $viewHelper132->initializeArgumentsAndRender();

$output124 .= '
								';
return $output124;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper133->setArguments($arguments122);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure123);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output97 .= $viewHelper133->initializeArgumentsAndRender();

$output97 .= '
							';
return $output97;
};
$viewHelper134 = $self->getViewHelper('$viewHelper134', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper');
$viewHelper134->setArguments($arguments94);
$viewHelper134->setRenderingContext($renderingContext);
$viewHelper134->setRenderChildrenClosure($renderChildrenClosure96);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper

$output93 .= $viewHelper134->initializeArgumentsAndRender();

$output93 .= '
						';
return $output93;
};
$viewHelper135 = $self->getViewHelper('$viewHelper135', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper135->setArguments($arguments91);
$viewHelper135->setRenderingContext($renderingContext);
$viewHelper135->setRenderChildrenClosure($renderChildrenClosure92);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output90 .= $viewHelper135->initializeArgumentsAndRender();

$output90 .= '
					';
return $output90;
};
$arguments23['__elseClosure'] = function() use ($renderingContext, $self) {
$output136 = '';

$output136 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments137 = array();
// Rendering Boolean node
$arguments137['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments137['then'] = NULL;
$arguments137['else'] = NULL;
$renderChildrenClosure138 = function() use ($renderingContext, $self) {
$output139 = '';

$output139 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments140 = array();
$arguments140['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments140['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments140['additionalAttributes'] = NULL;
$arguments140['uriOnly'] = false;
$arguments140['configuration'] = array (
);
$arguments140['class'] = NULL;
$arguments140['dir'] = NULL;
$arguments140['id'] = NULL;
$arguments140['lang'] = NULL;
$arguments140['style'] = NULL;
$arguments140['title'] = NULL;
$arguments140['accesskey'] = NULL;
$arguments140['tabindex'] = NULL;
$arguments140['onclick'] = NULL;
$arguments140['target'] = NULL;
$arguments140['rel'] = NULL;
$renderChildrenClosure141 = function() use ($renderingContext, $self) {
$output142 = '';

$output142 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments143 = array();
$arguments143['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments143['title'] = '';
$arguments143['alt'] = '';
$arguments143['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments143['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments143['additionalAttributes'] = NULL;
$arguments143['width'] = NULL;
$arguments143['height'] = NULL;
$arguments143['minWidth'] = NULL;
$arguments143['minHeight'] = NULL;
$arguments143['treatIdAsReference'] = false;
$arguments143['class'] = NULL;
$arguments143['dir'] = NULL;
$arguments143['id'] = NULL;
$arguments143['lang'] = NULL;
$arguments143['style'] = NULL;
$arguments143['accesskey'] = NULL;
$arguments143['tabindex'] = NULL;
$arguments143['onclick'] = NULL;
$arguments143['ismap'] = NULL;
$arguments143['longdesc'] = NULL;
$arguments143['usemap'] = NULL;
$renderChildrenClosure144 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper145 = $self->getViewHelper('$viewHelper145', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper145->setArguments($arguments143);
$viewHelper145->setRenderingContext($renderingContext);
$viewHelper145->setRenderChildrenClosure($renderChildrenClosure144);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output142 .= $viewHelper145->initializeArgumentsAndRender();

$output142 .= '
								';
return $output142;
};
$viewHelper146 = $self->getViewHelper('$viewHelper146', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper146->setArguments($arguments140);
$viewHelper146->setRenderingContext($renderingContext);
$viewHelper146->setRenderChildrenClosure($renderChildrenClosure141);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output139 .= $viewHelper146->initializeArgumentsAndRender();

$output139 .= '
							</span>
						';
return $output139;
};
$viewHelper147 = $self->getViewHelper('$viewHelper147', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper147->setArguments($arguments137);
$viewHelper147->setRenderingContext($renderingContext);
$viewHelper147->setRenderChildrenClosure($renderChildrenClosure138);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output136 .= $viewHelper147->initializeArgumentsAndRender();

$output136 .= '
					';
return $output136;
};
$viewHelper148 = $self->getViewHelper('$viewHelper148', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper148->setArguments($arguments23);
$viewHelper148->setRenderingContext($renderingContext);
$viewHelper148->setRenderChildrenClosure($renderChildrenClosure24);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output22 .= $viewHelper148->initializeArgumentsAndRender();

$output22 .= '

			</div>
		';
return $output22;
};
$viewHelper149 = $self->getViewHelper('$viewHelper149', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper149->setArguments($arguments20);
$viewHelper149->setRenderingContext($renderingContext);
$viewHelper149->setRenderChildrenClosure($renderChildrenClosure21);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output19 .= $viewHelper149->initializeArgumentsAndRender();

$output19 .= '
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments150 = array();
$renderChildrenClosure151 = function() use ($renderingContext, $self) {
$output152 = '';

$output152 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments153 = array();
// Rendering Boolean node
$arguments153['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments153['then'] = NULL;
$arguments153['else'] = NULL;
$renderChildrenClosure154 = function() use ($renderingContext, $self) {
$output155 = '';

$output155 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments156 = array();
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
$output158 = '';

$output158 .= '
					<div class="news-img-wrap">
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments159 = array();
$arguments159['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments159['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments159['additionalAttributes'] = NULL;
$arguments159['uriOnly'] = false;
$arguments159['configuration'] = array (
);
$arguments159['class'] = NULL;
$arguments159['dir'] = NULL;
$arguments159['id'] = NULL;
$arguments159['lang'] = NULL;
$arguments159['style'] = NULL;
$arguments159['title'] = NULL;
$arguments159['accesskey'] = NULL;
$arguments159['tabindex'] = NULL;
$arguments159['onclick'] = NULL;
$arguments159['target'] = NULL;
$arguments159['rel'] = NULL;
$renderChildrenClosure160 = function() use ($renderingContext, $self) {
$output161 = '';

$output161 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments162 = array();
$arguments162['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments162['title'] = '';
$arguments162['alt'] = '';
$arguments162['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments162['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments162['additionalAttributes'] = NULL;
$arguments162['width'] = NULL;
$arguments162['height'] = NULL;
$arguments162['minWidth'] = NULL;
$arguments162['minHeight'] = NULL;
$arguments162['treatIdAsReference'] = false;
$arguments162['class'] = NULL;
$arguments162['dir'] = NULL;
$arguments162['id'] = NULL;
$arguments162['lang'] = NULL;
$arguments162['style'] = NULL;
$arguments162['accesskey'] = NULL;
$arguments162['tabindex'] = NULL;
$arguments162['onclick'] = NULL;
$arguments162['ismap'] = NULL;
$arguments162['longdesc'] = NULL;
$arguments162['usemap'] = NULL;
$renderChildrenClosure163 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper164 = $self->getViewHelper('$viewHelper164', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper164->setArguments($arguments162);
$viewHelper164->setRenderingContext($renderingContext);
$viewHelper164->setRenderChildrenClosure($renderChildrenClosure163);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output161 .= $viewHelper164->initializeArgumentsAndRender();

$output161 .= '
							</span>
						';
return $output161;
};
$viewHelper165 = $self->getViewHelper('$viewHelper165', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper165->setArguments($arguments159);
$viewHelper165->setRenderingContext($renderingContext);
$viewHelper165->setRenderChildrenClosure($renderChildrenClosure160);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output158 .= $viewHelper165->initializeArgumentsAndRender();

$output158 .= '
					</div>
				';
return $output158;
};
$viewHelper166 = $self->getViewHelper('$viewHelper166', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper166->setArguments($arguments156);
$viewHelper166->setRenderingContext($renderingContext);
$viewHelper166->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output155 .= $viewHelper166->initializeArgumentsAndRender();

$output155 .= '
			';
return $output155;
};
$arguments153['__thenClosure'] = function() use ($renderingContext, $self) {
$output167 = '';

$output167 .= '
					<div class="news-img-wrap">
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments168 = array();
$arguments168['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments168['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments168['additionalAttributes'] = NULL;
$arguments168['uriOnly'] = false;
$arguments168['configuration'] = array (
);
$arguments168['class'] = NULL;
$arguments168['dir'] = NULL;
$arguments168['id'] = NULL;
$arguments168['lang'] = NULL;
$arguments168['style'] = NULL;
$arguments168['title'] = NULL;
$arguments168['accesskey'] = NULL;
$arguments168['tabindex'] = NULL;
$arguments168['onclick'] = NULL;
$arguments168['target'] = NULL;
$arguments168['rel'] = NULL;
$renderChildrenClosure169 = function() use ($renderingContext, $self) {
$output170 = '';

$output170 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments171 = array();
$arguments171['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments171['title'] = '';
$arguments171['alt'] = '';
$arguments171['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments171['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments171['additionalAttributes'] = NULL;
$arguments171['width'] = NULL;
$arguments171['height'] = NULL;
$arguments171['minWidth'] = NULL;
$arguments171['minHeight'] = NULL;
$arguments171['treatIdAsReference'] = false;
$arguments171['class'] = NULL;
$arguments171['dir'] = NULL;
$arguments171['id'] = NULL;
$arguments171['lang'] = NULL;
$arguments171['style'] = NULL;
$arguments171['accesskey'] = NULL;
$arguments171['tabindex'] = NULL;
$arguments171['onclick'] = NULL;
$arguments171['ismap'] = NULL;
$arguments171['longdesc'] = NULL;
$arguments171['usemap'] = NULL;
$renderChildrenClosure172 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper173 = $self->getViewHelper('$viewHelper173', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper173->setArguments($arguments171);
$viewHelper173->setRenderingContext($renderingContext);
$viewHelper173->setRenderChildrenClosure($renderChildrenClosure172);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output170 .= $viewHelper173->initializeArgumentsAndRender();

$output170 .= '
							</span>
						';
return $output170;
};
$viewHelper174 = $self->getViewHelper('$viewHelper174', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper174->setArguments($arguments168);
$viewHelper174->setRenderingContext($renderingContext);
$viewHelper174->setRenderChildrenClosure($renderChildrenClosure169);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output167 .= $viewHelper174->initializeArgumentsAndRender();

$output167 .= '
					</div>
				';
return $output167;
};
$viewHelper175 = $self->getViewHelper('$viewHelper175', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper175->setArguments($arguments153);
$viewHelper175->setRenderingContext($renderingContext);
$viewHelper175->setRenderChildrenClosure($renderChildrenClosure154);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output152 .= $viewHelper175->initializeArgumentsAndRender();

$output152 .= '
		';
return $output152;
};
$viewHelper176 = $self->getViewHelper('$viewHelper176', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper176->setArguments($arguments150);
$viewHelper176->setRenderingContext($renderingContext);
$viewHelper176->setRenderChildrenClosure($renderChildrenClosure151);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output19 .= $viewHelper176->initializeArgumentsAndRender();

$output19 .= '
	';
return $output19;
};
$arguments17['__thenClosure'] = function() use ($renderingContext, $self) {
$output177 = '';

$output177 .= '
			<div class="news-img-wrap">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments178 = array();
// Rendering Boolean node
$arguments178['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews', $renderingContext));
$arguments178['then'] = NULL;
$arguments178['else'] = NULL;
$renderChildrenClosure179 = function() use ($renderingContext, $self) {
$output180 = '';

$output180 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments181 = array();
$renderChildrenClosure182 = function() use ($renderingContext, $self) {
$output183 = '';

$output183 .= '
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments184 = array();
$arguments184['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments184['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments184['additionalAttributes'] = NULL;
$arguments184['uriOnly'] = false;
$arguments184['configuration'] = array (
);
$arguments184['class'] = NULL;
$arguments184['dir'] = NULL;
$arguments184['id'] = NULL;
$arguments184['lang'] = NULL;
$arguments184['style'] = NULL;
$arguments184['title'] = NULL;
$arguments184['accesskey'] = NULL;
$arguments184['tabindex'] = NULL;
$arguments184['onclick'] = NULL;
$arguments184['target'] = NULL;
$arguments184['rel'] = NULL;
$renderChildrenClosure185 = function() use ($renderingContext, $self) {
$output186 = '';

$output186 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper
$arguments187 = array();
// Rendering Array
$array188 = array();
$array188['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews.0', $renderingContext);
$arguments187['map'] = $array188;
$renderChildrenClosure189 = function() use ($renderingContext, $self) {
$output190 = '';

$output190 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments191 = array();
// Rendering Boolean node
$arguments191['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 0);
$arguments191['then'] = NULL;
$arguments191['else'] = NULL;
$renderChildrenClosure192 = function() use ($renderingContext, $self) {
$output193 = '';

$output193 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments194 = array();
$output195 = '';

$output195 .= 'uploads/tx_news/';

$output195 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.image', $renderingContext);
$arguments194['src'] = $output195;
$arguments194['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.title', $renderingContext);
$arguments194['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.alt', $renderingContext);
$arguments194['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments194['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments194['additionalAttributes'] = NULL;
$arguments194['width'] = NULL;
$arguments194['height'] = NULL;
$arguments194['minWidth'] = NULL;
$arguments194['minHeight'] = NULL;
$arguments194['treatIdAsReference'] = false;
$arguments194['class'] = NULL;
$arguments194['dir'] = NULL;
$arguments194['id'] = NULL;
$arguments194['lang'] = NULL;
$arguments194['style'] = NULL;
$arguments194['accesskey'] = NULL;
$arguments194['tabindex'] = NULL;
$arguments194['onclick'] = NULL;
$arguments194['ismap'] = NULL;
$arguments194['longdesc'] = NULL;
$arguments194['usemap'] = NULL;
$renderChildrenClosure196 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper197 = $self->getViewHelper('$viewHelper197', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper197->setArguments($arguments194);
$viewHelper197->setRenderingContext($renderingContext);
$viewHelper197->setRenderChildrenClosure($renderChildrenClosure196);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output193 .= $viewHelper197->initializeArgumentsAndRender();

$output193 .= '
								';
return $output193;
};
$viewHelper198 = $self->getViewHelper('$viewHelper198', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper198->setArguments($arguments191);
$viewHelper198->setRenderingContext($renderingContext);
$viewHelper198->setRenderChildrenClosure($renderChildrenClosure192);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output190 .= $viewHelper198->initializeArgumentsAndRender();

$output190 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments199 = array();
// Rendering Boolean node
$arguments199['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 1);
$arguments199['then'] = NULL;
$arguments199['else'] = NULL;
$renderChildrenClosure200 = function() use ($renderingContext, $self) {
$output201 = '';

$output201 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments202 = array();
$arguments202['partial'] = 'Detail/MediaVideo';
// Rendering Array
$array203 = array();
$array203['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments202['arguments'] = $array203;
$arguments202['section'] = NULL;
$arguments202['optional'] = false;
$renderChildrenClosure204 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper205 = $self->getViewHelper('$viewHelper205', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper205->setArguments($arguments202);
$viewHelper205->setRenderingContext($renderingContext);
$viewHelper205->setRenderChildrenClosure($renderChildrenClosure204);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output201 .= $viewHelper205->initializeArgumentsAndRender();

$output201 .= '
								';
return $output201;
};
$viewHelper206 = $self->getViewHelper('$viewHelper206', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper206->setArguments($arguments199);
$viewHelper206->setRenderingContext($renderingContext);
$viewHelper206->setRenderChildrenClosure($renderChildrenClosure200);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output190 .= $viewHelper206->initializeArgumentsAndRender();

$output190 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments207 = array();
// Rendering Boolean node
$arguments207['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 2);
$arguments207['then'] = NULL;
$arguments207['else'] = NULL;
$renderChildrenClosure208 = function() use ($renderingContext, $self) {
$output209 = '';

$output209 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments210 = array();
$arguments210['partial'] = 'Detail/MediaHtml';
// Rendering Array
$array211 = array();
$array211['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments210['arguments'] = $array211;
$arguments210['section'] = NULL;
$arguments210['optional'] = false;
$renderChildrenClosure212 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper213 = $self->getViewHelper('$viewHelper213', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper213->setArguments($arguments210);
$viewHelper213->setRenderingContext($renderingContext);
$viewHelper213->setRenderChildrenClosure($renderChildrenClosure212);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output209 .= $viewHelper213->initializeArgumentsAndRender();

$output209 .= '
								';
return $output209;
};
$viewHelper214 = $self->getViewHelper('$viewHelper214', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper214->setArguments($arguments207);
$viewHelper214->setRenderingContext($renderingContext);
$viewHelper214->setRenderChildrenClosure($renderChildrenClosure208);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output190 .= $viewHelper214->initializeArgumentsAndRender();

$output190 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments215 = array();
// Rendering Boolean node
$arguments215['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 3);
$arguments215['then'] = NULL;
$arguments215['else'] = NULL;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
$output217 = '';

$output217 .= '
									';
// Rendering ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper
$arguments218 = array();
$arguments218['as'] = 'dam';
$arguments218['uid'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.uid', $renderingContext);
$renderChildrenClosure219 = function() use ($renderingContext, $self) {
$output220 = '';

$output220 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments221 = array();
$output222 = '';

$output222 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_path', $renderingContext);

$output222 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_name', $renderingContext);
$arguments221['src'] = $output222;
$arguments221['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.title', $renderingContext);
$arguments221['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.alt_text', $renderingContext);
$arguments221['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments221['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments221['additionalAttributes'] = NULL;
$arguments221['width'] = NULL;
$arguments221['height'] = NULL;
$arguments221['minWidth'] = NULL;
$arguments221['minHeight'] = NULL;
$arguments221['treatIdAsReference'] = false;
$arguments221['class'] = NULL;
$arguments221['dir'] = NULL;
$arguments221['id'] = NULL;
$arguments221['lang'] = NULL;
$arguments221['style'] = NULL;
$arguments221['accesskey'] = NULL;
$arguments221['tabindex'] = NULL;
$arguments221['onclick'] = NULL;
$arguments221['ismap'] = NULL;
$arguments221['longdesc'] = NULL;
$arguments221['usemap'] = NULL;
$renderChildrenClosure223 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper224 = $self->getViewHelper('$viewHelper224', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper224->setArguments($arguments221);
$viewHelper224->setRenderingContext($renderingContext);
$viewHelper224->setRenderChildrenClosure($renderChildrenClosure223);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output220 .= $viewHelper224->initializeArgumentsAndRender();

$output220 .= '
									';
return $output220;
};
$viewHelper225 = $self->getViewHelper('$viewHelper225', $renderingContext, 'Tx_News_ViewHelpers_Format_DamViewHelper');
$viewHelper225->setArguments($arguments218);
$viewHelper225->setRenderingContext($renderingContext);
$viewHelper225->setRenderChildrenClosure($renderChildrenClosure219);
// End of ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper

$output217 .= $viewHelper225->initializeArgumentsAndRender();

$output217 .= '
								';
return $output217;
};
$viewHelper226 = $self->getViewHelper('$viewHelper226', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper226->setArguments($arguments215);
$viewHelper226->setRenderingContext($renderingContext);
$viewHelper226->setRenderChildrenClosure($renderChildrenClosure216);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output190 .= $viewHelper226->initializeArgumentsAndRender();

$output190 .= '
							';
return $output190;
};
$viewHelper227 = $self->getViewHelper('$viewHelper227', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper');
$viewHelper227->setArguments($arguments187);
$viewHelper227->setRenderingContext($renderingContext);
$viewHelper227->setRenderChildrenClosure($renderChildrenClosure189);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper

$output186 .= $viewHelper227->initializeArgumentsAndRender();

$output186 .= '
						';
return $output186;
};
$viewHelper228 = $self->getViewHelper('$viewHelper228', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper228->setArguments($arguments184);
$viewHelper228->setRenderingContext($renderingContext);
$viewHelper228->setRenderChildrenClosure($renderChildrenClosure185);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output183 .= $viewHelper228->initializeArgumentsAndRender();

$output183 .= '
					';
return $output183;
};
$viewHelper229 = $self->getViewHelper('$viewHelper229', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper229->setArguments($arguments181);
$viewHelper229->setRenderingContext($renderingContext);
$viewHelper229->setRenderChildrenClosure($renderChildrenClosure182);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output180 .= $viewHelper229->initializeArgumentsAndRender();

$output180 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments230 = array();
$renderChildrenClosure231 = function() use ($renderingContext, $self) {
$output232 = '';

$output232 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments233 = array();
// Rendering Boolean node
$arguments233['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments233['then'] = NULL;
$arguments233['else'] = NULL;
$renderChildrenClosure234 = function() use ($renderingContext, $self) {
$output235 = '';

$output235 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments236 = array();
$arguments236['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments236['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments236['additionalAttributes'] = NULL;
$arguments236['uriOnly'] = false;
$arguments236['configuration'] = array (
);
$arguments236['class'] = NULL;
$arguments236['dir'] = NULL;
$arguments236['id'] = NULL;
$arguments236['lang'] = NULL;
$arguments236['style'] = NULL;
$arguments236['title'] = NULL;
$arguments236['accesskey'] = NULL;
$arguments236['tabindex'] = NULL;
$arguments236['onclick'] = NULL;
$arguments236['target'] = NULL;
$arguments236['rel'] = NULL;
$renderChildrenClosure237 = function() use ($renderingContext, $self) {
$output238 = '';

$output238 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments239 = array();
$arguments239['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments239['title'] = '';
$arguments239['alt'] = '';
$arguments239['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments239['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments239['additionalAttributes'] = NULL;
$arguments239['width'] = NULL;
$arguments239['height'] = NULL;
$arguments239['minWidth'] = NULL;
$arguments239['minHeight'] = NULL;
$arguments239['treatIdAsReference'] = false;
$arguments239['class'] = NULL;
$arguments239['dir'] = NULL;
$arguments239['id'] = NULL;
$arguments239['lang'] = NULL;
$arguments239['style'] = NULL;
$arguments239['accesskey'] = NULL;
$arguments239['tabindex'] = NULL;
$arguments239['onclick'] = NULL;
$arguments239['ismap'] = NULL;
$arguments239['longdesc'] = NULL;
$arguments239['usemap'] = NULL;
$renderChildrenClosure240 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper241 = $self->getViewHelper('$viewHelper241', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper241->setArguments($arguments239);
$viewHelper241->setRenderingContext($renderingContext);
$viewHelper241->setRenderChildrenClosure($renderChildrenClosure240);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output238 .= $viewHelper241->initializeArgumentsAndRender();

$output238 .= '
								';
return $output238;
};
$viewHelper242 = $self->getViewHelper('$viewHelper242', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper242->setArguments($arguments236);
$viewHelper242->setRenderingContext($renderingContext);
$viewHelper242->setRenderChildrenClosure($renderChildrenClosure237);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output235 .= $viewHelper242->initializeArgumentsAndRender();

$output235 .= '
							</span>
						';
return $output235;
};
$viewHelper243 = $self->getViewHelper('$viewHelper243', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper243->setArguments($arguments233);
$viewHelper243->setRenderingContext($renderingContext);
$viewHelper243->setRenderChildrenClosure($renderChildrenClosure234);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output232 .= $viewHelper243->initializeArgumentsAndRender();

$output232 .= '
					';
return $output232;
};
$viewHelper244 = $self->getViewHelper('$viewHelper244', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper244->setArguments($arguments230);
$viewHelper244->setRenderingContext($renderingContext);
$viewHelper244->setRenderChildrenClosure($renderChildrenClosure231);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output180 .= $viewHelper244->initializeArgumentsAndRender();

$output180 .= '
				';
return $output180;
};
$arguments178['__thenClosure'] = function() use ($renderingContext, $self) {
$output245 = '';

$output245 .= '
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments246 = array();
$arguments246['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments246['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments246['additionalAttributes'] = NULL;
$arguments246['uriOnly'] = false;
$arguments246['configuration'] = array (
);
$arguments246['class'] = NULL;
$arguments246['dir'] = NULL;
$arguments246['id'] = NULL;
$arguments246['lang'] = NULL;
$arguments246['style'] = NULL;
$arguments246['title'] = NULL;
$arguments246['accesskey'] = NULL;
$arguments246['tabindex'] = NULL;
$arguments246['onclick'] = NULL;
$arguments246['target'] = NULL;
$arguments246['rel'] = NULL;
$renderChildrenClosure247 = function() use ($renderingContext, $self) {
$output248 = '';

$output248 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper
$arguments249 = array();
// Rendering Array
$array250 = array();
$array250['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.mediaPreviews.0', $renderingContext);
$arguments249['map'] = $array250;
$renderChildrenClosure251 = function() use ($renderingContext, $self) {
$output252 = '';

$output252 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments253 = array();
// Rendering Boolean node
$arguments253['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 0);
$arguments253['then'] = NULL;
$arguments253['else'] = NULL;
$renderChildrenClosure254 = function() use ($renderingContext, $self) {
$output255 = '';

$output255 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments256 = array();
$output257 = '';

$output257 .= 'uploads/tx_news/';

$output257 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.image', $renderingContext);
$arguments256['src'] = $output257;
$arguments256['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.title', $renderingContext);
$arguments256['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.alt', $renderingContext);
$arguments256['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments256['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments256['additionalAttributes'] = NULL;
$arguments256['width'] = NULL;
$arguments256['height'] = NULL;
$arguments256['minWidth'] = NULL;
$arguments256['minHeight'] = NULL;
$arguments256['treatIdAsReference'] = false;
$arguments256['class'] = NULL;
$arguments256['dir'] = NULL;
$arguments256['id'] = NULL;
$arguments256['lang'] = NULL;
$arguments256['style'] = NULL;
$arguments256['accesskey'] = NULL;
$arguments256['tabindex'] = NULL;
$arguments256['onclick'] = NULL;
$arguments256['ismap'] = NULL;
$arguments256['longdesc'] = NULL;
$arguments256['usemap'] = NULL;
$renderChildrenClosure258 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper259 = $self->getViewHelper('$viewHelper259', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper259->setArguments($arguments256);
$viewHelper259->setRenderingContext($renderingContext);
$viewHelper259->setRenderChildrenClosure($renderChildrenClosure258);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output255 .= $viewHelper259->initializeArgumentsAndRender();

$output255 .= '
								';
return $output255;
};
$viewHelper260 = $self->getViewHelper('$viewHelper260', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper260->setArguments($arguments253);
$viewHelper260->setRenderingContext($renderingContext);
$viewHelper260->setRenderChildrenClosure($renderChildrenClosure254);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper260->initializeArgumentsAndRender();

$output252 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments261 = array();
// Rendering Boolean node
$arguments261['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 1);
$arguments261['then'] = NULL;
$arguments261['else'] = NULL;
$renderChildrenClosure262 = function() use ($renderingContext, $self) {
$output263 = '';

$output263 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments264 = array();
$arguments264['partial'] = 'Detail/MediaVideo';
// Rendering Array
$array265 = array();
$array265['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments264['arguments'] = $array265;
$arguments264['section'] = NULL;
$arguments264['optional'] = false;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper267 = $self->getViewHelper('$viewHelper267', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper267->setArguments($arguments264);
$viewHelper267->setRenderingContext($renderingContext);
$viewHelper267->setRenderChildrenClosure($renderChildrenClosure266);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output263 .= $viewHelper267->initializeArgumentsAndRender();

$output263 .= '
								';
return $output263;
};
$viewHelper268 = $self->getViewHelper('$viewHelper268', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper268->setArguments($arguments261);
$viewHelper268->setRenderingContext($renderingContext);
$viewHelper268->setRenderChildrenClosure($renderChildrenClosure262);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper268->initializeArgumentsAndRender();

$output252 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments269 = array();
// Rendering Boolean node
$arguments269['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 2);
$arguments269['then'] = NULL;
$arguments269['else'] = NULL;
$renderChildrenClosure270 = function() use ($renderingContext, $self) {
$output271 = '';

$output271 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments272 = array();
$arguments272['partial'] = 'Detail/MediaHtml';
// Rendering Array
$array273 = array();
$array273['mediaElement'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement', $renderingContext);
$arguments272['arguments'] = $array273;
$arguments272['section'] = NULL;
$arguments272['optional'] = false;
$renderChildrenClosure274 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper275 = $self->getViewHelper('$viewHelper275', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper275->setArguments($arguments272);
$viewHelper275->setRenderingContext($renderingContext);
$viewHelper275->setRenderChildrenClosure($renderChildrenClosure274);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output271 .= $viewHelper275->initializeArgumentsAndRender();

$output271 .= '
								';
return $output271;
};
$viewHelper276 = $self->getViewHelper('$viewHelper276', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper276->setArguments($arguments269);
$viewHelper276->setRenderingContext($renderingContext);
$viewHelper276->setRenderChildrenClosure($renderChildrenClosure270);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper276->initializeArgumentsAndRender();

$output252 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments277 = array();
// Rendering Boolean node
$arguments277['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('==', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.type', $renderingContext), 3);
$arguments277['then'] = NULL;
$arguments277['else'] = NULL;
$renderChildrenClosure278 = function() use ($renderingContext, $self) {
$output279 = '';

$output279 .= '
									';
// Rendering ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper
$arguments280 = array();
$arguments280['as'] = 'dam';
$arguments280['uid'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'mediaElement.uid', $renderingContext);
$renderChildrenClosure281 = function() use ($renderingContext, $self) {
$output282 = '';

$output282 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments283 = array();
$output284 = '';

$output284 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_path', $renderingContext);

$output284 .= TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.file_name', $renderingContext);
$arguments283['src'] = $output284;
$arguments283['title'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.title', $renderingContext);
$arguments283['alt'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'dam.alt_text', $renderingContext);
$arguments283['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments283['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments283['additionalAttributes'] = NULL;
$arguments283['width'] = NULL;
$arguments283['height'] = NULL;
$arguments283['minWidth'] = NULL;
$arguments283['minHeight'] = NULL;
$arguments283['treatIdAsReference'] = false;
$arguments283['class'] = NULL;
$arguments283['dir'] = NULL;
$arguments283['id'] = NULL;
$arguments283['lang'] = NULL;
$arguments283['style'] = NULL;
$arguments283['accesskey'] = NULL;
$arguments283['tabindex'] = NULL;
$arguments283['onclick'] = NULL;
$arguments283['ismap'] = NULL;
$arguments283['longdesc'] = NULL;
$arguments283['usemap'] = NULL;
$renderChildrenClosure285 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper286 = $self->getViewHelper('$viewHelper286', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper286->setArguments($arguments283);
$viewHelper286->setRenderingContext($renderingContext);
$viewHelper286->setRenderChildrenClosure($renderChildrenClosure285);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output282 .= $viewHelper286->initializeArgumentsAndRender();

$output282 .= '
									';
return $output282;
};
$viewHelper287 = $self->getViewHelper('$viewHelper287', $renderingContext, 'Tx_News_ViewHelpers_Format_DamViewHelper');
$viewHelper287->setArguments($arguments280);
$viewHelper287->setRenderingContext($renderingContext);
$viewHelper287->setRenderChildrenClosure($renderChildrenClosure281);
// End of ViewHelper Tx_News_ViewHelpers_Format_DamViewHelper

$output279 .= $viewHelper287->initializeArgumentsAndRender();

$output279 .= '
								';
return $output279;
};
$viewHelper288 = $self->getViewHelper('$viewHelper288', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper288->setArguments($arguments277);
$viewHelper288->setRenderingContext($renderingContext);
$viewHelper288->setRenderChildrenClosure($renderChildrenClosure278);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper288->initializeArgumentsAndRender();

$output252 .= '
							';
return $output252;
};
$viewHelper289 = $self->getViewHelper('$viewHelper289', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper');
$viewHelper289->setArguments($arguments249);
$viewHelper289->setRenderingContext($renderingContext);
$viewHelper289->setRenderChildrenClosure($renderChildrenClosure251);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\AliasViewHelper

$output248 .= $viewHelper289->initializeArgumentsAndRender();

$output248 .= '
						';
return $output248;
};
$viewHelper290 = $self->getViewHelper('$viewHelper290', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper290->setArguments($arguments246);
$viewHelper290->setRenderingContext($renderingContext);
$viewHelper290->setRenderChildrenClosure($renderChildrenClosure247);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output245 .= $viewHelper290->initializeArgumentsAndRender();

$output245 .= '
					';
return $output245;
};
$arguments178['__elseClosure'] = function() use ($renderingContext, $self) {
$output291 = '';

$output291 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments292 = array();
// Rendering Boolean node
$arguments292['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments292['then'] = NULL;
$arguments292['else'] = NULL;
$renderChildrenClosure293 = function() use ($renderingContext, $self) {
$output294 = '';

$output294 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments295 = array();
$arguments295['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments295['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments295['additionalAttributes'] = NULL;
$arguments295['uriOnly'] = false;
$arguments295['configuration'] = array (
);
$arguments295['class'] = NULL;
$arguments295['dir'] = NULL;
$arguments295['id'] = NULL;
$arguments295['lang'] = NULL;
$arguments295['style'] = NULL;
$arguments295['title'] = NULL;
$arguments295['accesskey'] = NULL;
$arguments295['tabindex'] = NULL;
$arguments295['onclick'] = NULL;
$arguments295['target'] = NULL;
$arguments295['rel'] = NULL;
$renderChildrenClosure296 = function() use ($renderingContext, $self) {
$output297 = '';

$output297 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments298 = array();
$arguments298['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments298['title'] = '';
$arguments298['alt'] = '';
$arguments298['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments298['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments298['additionalAttributes'] = NULL;
$arguments298['width'] = NULL;
$arguments298['height'] = NULL;
$arguments298['minWidth'] = NULL;
$arguments298['minHeight'] = NULL;
$arguments298['treatIdAsReference'] = false;
$arguments298['class'] = NULL;
$arguments298['dir'] = NULL;
$arguments298['id'] = NULL;
$arguments298['lang'] = NULL;
$arguments298['style'] = NULL;
$arguments298['accesskey'] = NULL;
$arguments298['tabindex'] = NULL;
$arguments298['onclick'] = NULL;
$arguments298['ismap'] = NULL;
$arguments298['longdesc'] = NULL;
$arguments298['usemap'] = NULL;
$renderChildrenClosure299 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper300 = $self->getViewHelper('$viewHelper300', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper300->setArguments($arguments298);
$viewHelper300->setRenderingContext($renderingContext);
$viewHelper300->setRenderChildrenClosure($renderChildrenClosure299);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output297 .= $viewHelper300->initializeArgumentsAndRender();

$output297 .= '
								';
return $output297;
};
$viewHelper301 = $self->getViewHelper('$viewHelper301', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper301->setArguments($arguments295);
$viewHelper301->setRenderingContext($renderingContext);
$viewHelper301->setRenderChildrenClosure($renderChildrenClosure296);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output294 .= $viewHelper301->initializeArgumentsAndRender();

$output294 .= '
							</span>
						';
return $output294;
};
$viewHelper302 = $self->getViewHelper('$viewHelper302', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper302->setArguments($arguments292);
$viewHelper302->setRenderingContext($renderingContext);
$viewHelper302->setRenderChildrenClosure($renderChildrenClosure293);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output291 .= $viewHelper302->initializeArgumentsAndRender();

$output291 .= '
					';
return $output291;
};
$viewHelper303 = $self->getViewHelper('$viewHelper303', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper303->setArguments($arguments178);
$viewHelper303->setRenderingContext($renderingContext);
$viewHelper303->setRenderChildrenClosure($renderChildrenClosure179);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output177 .= $viewHelper303->initializeArgumentsAndRender();

$output177 .= '

			</div>
		';
return $output177;
};
$arguments17['__elseClosure'] = function() use ($renderingContext, $self) {
$output304 = '';

$output304 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments305 = array();
// Rendering Boolean node
$arguments305['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.displayDummyIfNoMedia', $renderingContext));
$arguments305['then'] = NULL;
$arguments305['else'] = NULL;
$renderChildrenClosure306 = function() use ($renderingContext, $self) {
$output307 = '';

$output307 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments308 = array();
$renderChildrenClosure309 = function() use ($renderingContext, $self) {
$output310 = '';

$output310 .= '
					<div class="news-img-wrap">
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments311 = array();
$arguments311['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments311['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments311['additionalAttributes'] = NULL;
$arguments311['uriOnly'] = false;
$arguments311['configuration'] = array (
);
$arguments311['class'] = NULL;
$arguments311['dir'] = NULL;
$arguments311['id'] = NULL;
$arguments311['lang'] = NULL;
$arguments311['style'] = NULL;
$arguments311['title'] = NULL;
$arguments311['accesskey'] = NULL;
$arguments311['tabindex'] = NULL;
$arguments311['onclick'] = NULL;
$arguments311['target'] = NULL;
$arguments311['rel'] = NULL;
$renderChildrenClosure312 = function() use ($renderingContext, $self) {
$output313 = '';

$output313 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments314 = array();
$arguments314['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments314['title'] = '';
$arguments314['alt'] = '';
$arguments314['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments314['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments314['additionalAttributes'] = NULL;
$arguments314['width'] = NULL;
$arguments314['height'] = NULL;
$arguments314['minWidth'] = NULL;
$arguments314['minHeight'] = NULL;
$arguments314['treatIdAsReference'] = false;
$arguments314['class'] = NULL;
$arguments314['dir'] = NULL;
$arguments314['id'] = NULL;
$arguments314['lang'] = NULL;
$arguments314['style'] = NULL;
$arguments314['accesskey'] = NULL;
$arguments314['tabindex'] = NULL;
$arguments314['onclick'] = NULL;
$arguments314['ismap'] = NULL;
$arguments314['longdesc'] = NULL;
$arguments314['usemap'] = NULL;
$renderChildrenClosure315 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper316 = $self->getViewHelper('$viewHelper316', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper316->setArguments($arguments314);
$viewHelper316->setRenderingContext($renderingContext);
$viewHelper316->setRenderChildrenClosure($renderChildrenClosure315);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output313 .= $viewHelper316->initializeArgumentsAndRender();

$output313 .= '
							</span>
						';
return $output313;
};
$viewHelper317 = $self->getViewHelper('$viewHelper317', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper317->setArguments($arguments311);
$viewHelper317->setRenderingContext($renderingContext);
$viewHelper317->setRenderChildrenClosure($renderChildrenClosure312);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output310 .= $viewHelper317->initializeArgumentsAndRender();

$output310 .= '
					</div>
				';
return $output310;
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper318->setArguments($arguments308);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure309);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output307 .= $viewHelper318->initializeArgumentsAndRender();

$output307 .= '
			';
return $output307;
};
$arguments305['__thenClosure'] = function() use ($renderingContext, $self) {
$output319 = '';

$output319 .= '
					<div class="news-img-wrap">
						';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments320 = array();
$arguments320['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments320['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments320['additionalAttributes'] = NULL;
$arguments320['uriOnly'] = false;
$arguments320['configuration'] = array (
);
$arguments320['class'] = NULL;
$arguments320['dir'] = NULL;
$arguments320['id'] = NULL;
$arguments320['lang'] = NULL;
$arguments320['style'] = NULL;
$arguments320['title'] = NULL;
$arguments320['accesskey'] = NULL;
$arguments320['tabindex'] = NULL;
$arguments320['onclick'] = NULL;
$arguments320['target'] = NULL;
$arguments320['rel'] = NULL;
$renderChildrenClosure321 = function() use ($renderingContext, $self) {
$output322 = '';

$output322 .= '
							<span class="no-media-element">
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper
$arguments323 = array();
$arguments323['src'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.dummyImage', $renderingContext);
$arguments323['title'] = '';
$arguments323['alt'] = '';
$arguments323['maxWidth'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxWidth', $renderingContext);
$arguments323['maxHeight'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.media.image.maxHeight', $renderingContext);
$arguments323['additionalAttributes'] = NULL;
$arguments323['width'] = NULL;
$arguments323['height'] = NULL;
$arguments323['minWidth'] = NULL;
$arguments323['minHeight'] = NULL;
$arguments323['treatIdAsReference'] = false;
$arguments323['class'] = NULL;
$arguments323['dir'] = NULL;
$arguments323['id'] = NULL;
$arguments323['lang'] = NULL;
$arguments323['style'] = NULL;
$arguments323['accesskey'] = NULL;
$arguments323['tabindex'] = NULL;
$arguments323['onclick'] = NULL;
$arguments323['ismap'] = NULL;
$arguments323['longdesc'] = NULL;
$arguments323['usemap'] = NULL;
$renderChildrenClosure324 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper325 = $self->getViewHelper('$viewHelper325', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper');
$viewHelper325->setArguments($arguments323);
$viewHelper325->setRenderingContext($renderingContext);
$viewHelper325->setRenderChildrenClosure($renderChildrenClosure324);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ImageViewHelper

$output322 .= $viewHelper325->initializeArgumentsAndRender();

$output322 .= '
							</span>
						';
return $output322;
};
$viewHelper326 = $self->getViewHelper('$viewHelper326', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper326->setArguments($arguments320);
$viewHelper326->setRenderingContext($renderingContext);
$viewHelper326->setRenderChildrenClosure($renderChildrenClosure321);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output319 .= $viewHelper326->initializeArgumentsAndRender();

$output319 .= '
					</div>
				';
return $output319;
};
$viewHelper327 = $self->getViewHelper('$viewHelper327', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper327->setArguments($arguments305);
$viewHelper327->setRenderingContext($renderingContext);
$viewHelper327->setRenderChildrenClosure($renderChildrenClosure306);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output304 .= $viewHelper327->initializeArgumentsAndRender();

$output304 .= '
		';
return $output304;
};
$viewHelper328 = $self->getViewHelper('$viewHelper328', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper328->setArguments($arguments17);
$viewHelper328->setRenderingContext($renderingContext);
$viewHelper328->setRenderChildrenClosure($renderChildrenClosure18);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper328->initializeArgumentsAndRender();

$output0 .= '

	<!-- teas=ser text -->
	<div class="teaser-text">
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments329 = array();
// Rendering Boolean node
$arguments329['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.teaser', $renderingContext));
$arguments329['then'] = NULL;
$arguments329['else'] = NULL;
$renderChildrenClosure330 = function() use ($renderingContext, $self) {
$output331 = '';

$output331 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments332 = array();
$renderChildrenClosure333 = function() use ($renderingContext, $self) {
$output334 = '';

$output334 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments335 = array();
$arguments335['parseFuncTSPath'] = 'lib.parseFunc_RTE';
$renderChildrenClosure336 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
$arguments337 = array();
$arguments337['maxCharacters'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.cropMaxCharacters', $renderingContext);
// Rendering Boolean node
$arguments337['respectWordBoundaries'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('1');
$arguments337['append'] = '...';
$arguments337['respectHtml'] = true;
$renderChildrenClosure338 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.teaser', $renderingContext);
};
$viewHelper339 = $self->getViewHelper('$viewHelper339', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper');
$viewHelper339->setArguments($arguments337);
$viewHelper339->setRenderingContext($renderingContext);
$viewHelper339->setRenderChildrenClosure($renderChildrenClosure338);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
return $viewHelper339->initializeArgumentsAndRender();
};
$viewHelper340 = $self->getViewHelper('$viewHelper340', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper340->setArguments($arguments335);
$viewHelper340->setRenderingContext($renderingContext);
$viewHelper340->setRenderChildrenClosure($renderChildrenClosure336);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output334 .= $viewHelper340->initializeArgumentsAndRender();

$output334 .= '
			';
return $output334;
};
$viewHelper341 = $self->getViewHelper('$viewHelper341', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper341->setArguments($arguments332);
$viewHelper341->setRenderingContext($renderingContext);
$viewHelper341->setRenderChildrenClosure($renderChildrenClosure333);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output331 .= $viewHelper341->initializeArgumentsAndRender();

$output331 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments342 = array();
$renderChildrenClosure343 = function() use ($renderingContext, $self) {
$output344 = '';

$output344 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments345 = array();
$arguments345['parseFuncTSPath'] = 'lib.parseFunc_RTE';
$renderChildrenClosure346 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
$arguments347 = array();
$arguments347['maxCharacters'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.cropMaxCharacters', $renderingContext);
// Rendering Boolean node
$arguments347['respectWordBoundaries'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('1');
$arguments347['append'] = '...';
$arguments347['respectHtml'] = true;
$renderChildrenClosure348 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.bodytext', $renderingContext);
};
$viewHelper349 = $self->getViewHelper('$viewHelper349', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper');
$viewHelper349->setArguments($arguments347);
$viewHelper349->setRenderingContext($renderingContext);
$viewHelper349->setRenderChildrenClosure($renderChildrenClosure348);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
return $viewHelper349->initializeArgumentsAndRender();
};
$viewHelper350 = $self->getViewHelper('$viewHelper350', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper350->setArguments($arguments345);
$viewHelper350->setRenderingContext($renderingContext);
$viewHelper350->setRenderChildrenClosure($renderChildrenClosure346);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output344 .= $viewHelper350->initializeArgumentsAndRender();

$output344 .= '
			';
return $output344;
};
$viewHelper351 = $self->getViewHelper('$viewHelper351', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper351->setArguments($arguments342);
$viewHelper351->setRenderingContext($renderingContext);
$viewHelper351->setRenderChildrenClosure($renderChildrenClosure343);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output331 .= $viewHelper351->initializeArgumentsAndRender();

$output331 .= '
		';
return $output331;
};
$arguments329['__thenClosure'] = function() use ($renderingContext, $self) {
$output352 = '';

$output352 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments353 = array();
$arguments353['parseFuncTSPath'] = 'lib.parseFunc_RTE';
$renderChildrenClosure354 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
$arguments355 = array();
$arguments355['maxCharacters'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.cropMaxCharacters', $renderingContext);
// Rendering Boolean node
$arguments355['respectWordBoundaries'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('1');
$arguments355['append'] = '...';
$arguments355['respectHtml'] = true;
$renderChildrenClosure356 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.teaser', $renderingContext);
};
$viewHelper357 = $self->getViewHelper('$viewHelper357', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper');
$viewHelper357->setArguments($arguments355);
$viewHelper357->setRenderingContext($renderingContext);
$viewHelper357->setRenderChildrenClosure($renderChildrenClosure356);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
return $viewHelper357->initializeArgumentsAndRender();
};
$viewHelper358 = $self->getViewHelper('$viewHelper358', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper358->setArguments($arguments353);
$viewHelper358->setRenderingContext($renderingContext);
$viewHelper358->setRenderChildrenClosure($renderChildrenClosure354);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output352 .= $viewHelper358->initializeArgumentsAndRender();

$output352 .= '
			';
return $output352;
};
$arguments329['__elseClosure'] = function() use ($renderingContext, $self) {
$output359 = '';

$output359 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper
$arguments360 = array();
$arguments360['parseFuncTSPath'] = 'lib.parseFunc_RTE';
$renderChildrenClosure361 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
$arguments362 = array();
$arguments362['maxCharacters'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.cropMaxCharacters', $renderingContext);
// Rendering Boolean node
$arguments362['respectWordBoundaries'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean('1');
$arguments362['append'] = '...';
$arguments362['respectHtml'] = true;
$renderChildrenClosure363 = function() use ($renderingContext, $self) {
return TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.bodytext', $renderingContext);
};
$viewHelper364 = $self->getViewHelper('$viewHelper364', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper');
$viewHelper364->setArguments($arguments362);
$viewHelper364->setRenderingContext($renderingContext);
$viewHelper364->setRenderChildrenClosure($renderChildrenClosure363);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\CropViewHelper
return $viewHelper364->initializeArgumentsAndRender();
};
$viewHelper365 = $self->getViewHelper('$viewHelper365', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper');
$viewHelper365->setArguments($arguments360);
$viewHelper365->setRenderingContext($renderingContext);
$viewHelper365->setRenderChildrenClosure($renderChildrenClosure361);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlViewHelper

$output359 .= $viewHelper365->initializeArgumentsAndRender();

$output359 .= '
			';
return $output359;
};
$viewHelper366 = $self->getViewHelper('$viewHelper366', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper366->setArguments($arguments329);
$viewHelper366->setRenderingContext($renderingContext);
$viewHelper366->setRenderChildrenClosure($renderChildrenClosure330);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper366->initializeArgumentsAndRender();

$output0 .= '

		';
// Rendering ViewHelper Tx_News_ViewHelpers_LinkViewHelper
$arguments367 = array();
$arguments367['newsItem'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem', $renderingContext);
$arguments367['settings'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings', $renderingContext);
$arguments367['class'] = 'more';
$arguments367['additionalAttributes'] = NULL;
$arguments367['uriOnly'] = false;
$arguments367['configuration'] = array (
);
$arguments367['dir'] = NULL;
$arguments367['id'] = NULL;
$arguments367['lang'] = NULL;
$arguments367['style'] = NULL;
$arguments367['title'] = NULL;
$arguments367['accesskey'] = NULL;
$arguments367['tabindex'] = NULL;
$arguments367['onclick'] = NULL;
$arguments367['target'] = NULL;
$arguments367['rel'] = NULL;
$renderChildrenClosure368 = function() use ($renderingContext, $self) {
$output369 = '';

$output369 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments370 = array();
$arguments370['key'] = 'more-link';
$arguments370['id'] = NULL;
$arguments370['default'] = NULL;
$arguments370['htmlEscape'] = NULL;
$arguments370['arguments'] = NULL;
$arguments370['extensionName'] = NULL;
$renderChildrenClosure371 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper372 = $self->getViewHelper('$viewHelper372', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper372->setArguments($arguments370);
$viewHelper372->setRenderingContext($renderingContext);
$viewHelper372->setRenderChildrenClosure($renderChildrenClosure371);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output369 .= $viewHelper372->initializeArgumentsAndRender();

$output369 .= '
		';
return $output369;
};
$viewHelper373 = $self->getViewHelper('$viewHelper373', $renderingContext, 'Tx_News_ViewHelpers_LinkViewHelper');
$viewHelper373->setArguments($arguments367);
$viewHelper373->setRenderingContext($renderingContext);
$viewHelper373->setRenderChildrenClosure($renderChildrenClosure368);
// End of ViewHelper Tx_News_ViewHelpers_LinkViewHelper

$output0 .= $viewHelper373->initializeArgumentsAndRender();

$output0 .= '
	</div>

	<!-- footer information -->
	<div class="footer">
		<p>
			<!-- date -->
			<span class="news-list-date">
				';
// Rendering ViewHelper Tx_News_ViewHelpers_Format_DateViewHelper
$arguments374 = array();
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments375 = array();
$arguments375['key'] = 'dateFormat';
$arguments375['id'] = NULL;
$arguments375['default'] = NULL;
$arguments375['htmlEscape'] = NULL;
$arguments375['arguments'] = NULL;
$arguments375['extensionName'] = NULL;
$renderChildrenClosure376 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper377 = $self->getViewHelper('$viewHelper377', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper377->setArguments($arguments375);
$viewHelper377->setRenderingContext($renderingContext);
$viewHelper377->setRenderChildrenClosure($renderChildrenClosure376);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments374['format'] = $viewHelper377->initializeArgumentsAndRender();
$arguments374['date'] = NULL;
$arguments374['currentDate'] = false;
$arguments374['strftime'] = true;
$renderChildrenClosure378 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments379 = array();
$arguments379['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.datetime', $renderingContext);
$arguments379['keepQuotes'] = false;
$arguments379['encoding'] = NULL;
$arguments379['doubleEncode'] = true;
$renderChildrenClosure380 = function() use ($renderingContext, $self) {
return NULL;
};
$value381 = ($arguments379['value'] !== NULL ? $arguments379['value'] : $renderChildrenClosure380());
return (!is_string($value381) ? $value381 : htmlspecialchars($value381, ($arguments379['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments379['encoding'] !== NULL ? $arguments379['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments379['doubleEncode']));
};
$viewHelper382 = $self->getViewHelper('$viewHelper382', $renderingContext, 'Tx_News_ViewHelpers_Format_DateViewHelper');
$viewHelper382->setArguments($arguments374);
$viewHelper382->setRenderingContext($renderingContext);
$viewHelper382->setRenderChildrenClosure($renderChildrenClosure378);
// End of ViewHelper Tx_News_ViewHelpers_Format_DateViewHelper

$output0 .= $viewHelper382->initializeArgumentsAndRender();

$output0 .= '
			</span>

			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments383 = array();
// Rendering Boolean node
$arguments383['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.firstCategory', $renderingContext));
$arguments383['then'] = NULL;
$arguments383['else'] = NULL;
$renderChildrenClosure384 = function() use ($renderingContext, $self) {
$output385 = '';

$output385 .= '
				<!-- first category -->
				<span class="news-list-category">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments386 = array();
$arguments386['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.firstCategory.title', $renderingContext);
$arguments386['keepQuotes'] = false;
$arguments386['encoding'] = NULL;
$arguments386['doubleEncode'] = true;
$renderChildrenClosure387 = function() use ($renderingContext, $self) {
return NULL;
};
$value388 = ($arguments386['value'] !== NULL ? $arguments386['value'] : $renderChildrenClosure387());

$output385 .= (!is_string($value388) ? $value388 : htmlspecialchars($value388, ($arguments386['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments386['encoding'] !== NULL ? $arguments386['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments386['doubleEncode']));

$output385 .= '</span>
			';
return $output385;
};
$viewHelper389 = $self->getViewHelper('$viewHelper389', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper389->setArguments($arguments383);
$viewHelper389->setRenderingContext($renderingContext);
$viewHelper389->setRenderChildrenClosure($renderChildrenClosure384);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper389->initializeArgumentsAndRender();

$output0 .= '

			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments390 = array();
// Rendering Boolean node
$arguments390['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.tags', $renderingContext));
$arguments390['then'] = NULL;
$arguments390['else'] = NULL;
$renderChildrenClosure391 = function() use ($renderingContext, $self) {
$output392 = '';

$output392 .= '
				<!-- Tags -->
				<span class="news-list-tags">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments393 = array();
$arguments393['each'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.tags', $renderingContext);
$arguments393['as'] = 'tag';
$arguments393['key'] = '';
$arguments393['reverse'] = false;
$arguments393['iteration'] = NULL;
$renderChildrenClosure394 = function() use ($renderingContext, $self) {
$output395 = '';

$output395 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments396 = array();
$arguments396['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'tag.title', $renderingContext);
$arguments396['keepQuotes'] = false;
$arguments396['encoding'] = NULL;
$arguments396['doubleEncode'] = true;
$renderChildrenClosure397 = function() use ($renderingContext, $self) {
return NULL;
};
$value398 = ($arguments396['value'] !== NULL ? $arguments396['value'] : $renderChildrenClosure397());

$output395 .= (!is_string($value398) ? $value398 : htmlspecialchars($value398, ($arguments396['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments396['encoding'] !== NULL ? $arguments396['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments396['doubleEncode']));

$output395 .= '
				';
return $output395;
};

$output392 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments393, $renderChildrenClosure394, $renderingContext);

$output392 .= '
				</span>
			';
return $output392;
};
$viewHelper399 = $self->getViewHelper('$viewHelper399', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper399->setArguments($arguments390);
$viewHelper399->setRenderingContext($renderingContext);
$viewHelper399->setRenderChildrenClosure($renderChildrenClosure391);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper399->initializeArgumentsAndRender();

$output0 .= '

			<!-- author -->
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments400 = array();
// Rendering Boolean node
$arguments400['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.author', $renderingContext));
$arguments400['then'] = NULL;
$arguments400['else'] = NULL;
$renderChildrenClosure401 = function() use ($renderingContext, $self) {
$output402 = '';

$output402 .= '
				<span class="news-list-author">
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments403 = array();
$arguments403['key'] = 'author';
// Rendering Array
$array404 = array();
$array404['0'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'newsItem.author', $renderingContext);
$arguments403['arguments'] = $array404;
$arguments403['id'] = NULL;
$arguments403['default'] = NULL;
$arguments403['htmlEscape'] = NULL;
$arguments403['extensionName'] = NULL;
$renderChildrenClosure405 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper406 = $self->getViewHelper('$viewHelper406', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper406->setArguments($arguments403);
$viewHelper406->setRenderingContext($renderingContext);
$viewHelper406->setRenderChildrenClosure($renderChildrenClosure405);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output402 .= $viewHelper406->initializeArgumentsAndRender();

$output402 .= '
				</span>
			';
return $output402;
};
$viewHelper407 = $self->getViewHelper('$viewHelper407', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper407->setArguments($arguments400);
$viewHelper407->setRenderingContext($renderingContext);
$viewHelper407->setRenderChildrenClosure($renderChildrenClosure401);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper407->initializeArgumentsAndRender();

$output0 .= '
		</p>
	</div>
</div>
';

return $output0;
}


}
#1399936121    127880    