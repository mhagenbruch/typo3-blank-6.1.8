<?php
class FluidCache_News_ViewHelpers_Widget_Paginate_action_index_669bcfb56d06f47353d185dd1271439b8d738009 extends \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate {

public function getVariableContainer() {
	// TODO
	return new \TYPO3\CMS\Fluid\Core\ViewHelper\TemplateVariableContainer();
}
public function getLayoutName(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {

return NULL;
}
public function hasLayout() {
return FALSE;
}

/**
 * section paginator
 */
public function section_31b8d545b1939b065e8931304bab52b99d8b4567(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output0 = '';

$output0 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments1 = array();
// Rendering Boolean node
$arguments1['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext), 1);
$arguments1['then'] = NULL;
$arguments1['else'] = NULL;
$renderChildrenClosure2 = function() use ($renderingContext, $self) {
$output3 = '';

$output3 .= '
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments4 = array();
// Rendering Boolean node
$arguments4['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.paginate', $renderingContext));
$arguments4['then'] = NULL;
$arguments4['else'] = NULL;
$renderChildrenClosure5 = function() use ($renderingContext, $self) {
$output6 = '';

$output6 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments7 = array();
// Rendering Boolean node
$arguments7['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.current', $renderingContext), 1);
$arguments7['then'] = NULL;
$arguments7['else'] = NULL;
$renderChildrenClosure8 = function() use ($renderingContext, $self) {
$output9 = '';

$output9 .= '
				';
// Rendering ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper
$arguments10 = array();
$renderChildrenClosure11 = function() use ($renderingContext, $self) {
$output12 = '';

$output12 .= '<link rel="prev" href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper
$arguments13 = array();
// Rendering Array
$array14 = array();
$array14['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments13['arguments'] = $array14;
$arguments13['action'] = NULL;
$arguments13['section'] = '';
$arguments13['format'] = '';
$arguments13['ajax'] = false;
$renderChildrenClosure15 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper16 = $self->getViewHelper('$viewHelper16', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper');
$viewHelper16->setArguments($arguments13);
$viewHelper16->setRenderingContext($renderingContext);
$viewHelper16->setRenderChildrenClosure($renderChildrenClosure15);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper

$output12 .= $viewHelper16->initializeArgumentsAndRender();

$output12 .= '" />';
return $output12;
};
$viewHelper17 = $self->getViewHelper('$viewHelper17', $renderingContext, 'Tx_News_ViewHelpers_HeaderDataViewHelper');
$viewHelper17->setArguments($arguments10);
$viewHelper17->setRenderingContext($renderingContext);
$viewHelper17->setRenderChildrenClosure($renderChildrenClosure11);
// End of ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper

$output9 .= $viewHelper17->initializeArgumentsAndRender();

$output9 .= '
			';
return $output9;
};
$viewHelper18 = $self->getViewHelper('$viewHelper18', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper18->setArguments($arguments7);
$viewHelper18->setRenderingContext($renderingContext);
$viewHelper18->setRenderChildrenClosure($renderChildrenClosure8);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper18->initializeArgumentsAndRender();

$output6 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments19 = array();
// Rendering Boolean node
$arguments19['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext));
$arguments19['then'] = NULL;
$arguments19['else'] = NULL;
$renderChildrenClosure20 = function() use ($renderingContext, $self) {
$output21 = '';

$output21 .= '
				';
// Rendering ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper
$arguments22 = array();
$renderChildrenClosure23 = function() use ($renderingContext, $self) {
$output24 = '';

$output24 .= '<link rel="next" href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper
$arguments25 = array();
// Rendering Array
$array26 = array();
$array26['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext);
$arguments25['arguments'] = $array26;
$arguments25['action'] = NULL;
$arguments25['section'] = '';
$arguments25['format'] = '';
$arguments25['ajax'] = false;
$renderChildrenClosure27 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper28 = $self->getViewHelper('$viewHelper28', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper');
$viewHelper28->setArguments($arguments25);
$viewHelper28->setRenderingContext($renderingContext);
$viewHelper28->setRenderChildrenClosure($renderChildrenClosure27);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper

$output24 .= $viewHelper28->initializeArgumentsAndRender();

$output24 .= '" />';
return $output24;
};
$viewHelper29 = $self->getViewHelper('$viewHelper29', $renderingContext, 'Tx_News_ViewHelpers_HeaderDataViewHelper');
$viewHelper29->setArguments($arguments22);
$viewHelper29->setRenderingContext($renderingContext);
$viewHelper29->setRenderChildrenClosure($renderChildrenClosure23);
// End of ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper

$output21 .= $viewHelper29->initializeArgumentsAndRender();

$output21 .= '
			';
return $output21;
};
$viewHelper30 = $self->getViewHelper('$viewHelper30', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper30->setArguments($arguments19);
$viewHelper30->setRenderingContext($renderingContext);
$viewHelper30->setRenderChildrenClosure($renderChildrenClosure20);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output6 .= $viewHelper30->initializeArgumentsAndRender();

$output6 .= '
		';
return $output6;
};
$viewHelper31 = $self->getViewHelper('$viewHelper31', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper31->setArguments($arguments4);
$viewHelper31->setRenderingContext($renderingContext);
$viewHelper31->setRenderChildrenClosure($renderChildrenClosure5);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper31->initializeArgumentsAndRender();

$output3 .= '

		<div class="page-navigation">
			<p>
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments32 = array();
$arguments32['key'] = 'paginate_overall';
// Rendering Array
$array33 = array();
$array33['0'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.current', $renderingContext);
$array33['1'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments32['arguments'] = $array33;
$arguments32['id'] = NULL;
$arguments32['default'] = NULL;
$arguments32['htmlEscape'] = NULL;
$arguments32['extensionName'] = NULL;
$renderChildrenClosure34 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper35 = $self->getViewHelper('$viewHelper35', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper35->setArguments($arguments32);
$viewHelper35->setRenderingContext($renderingContext);
$viewHelper35->setRenderChildrenClosure($renderChildrenClosure34);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output3 .= $viewHelper35->initializeArgumentsAndRender();

$output3 .= '
			</p>
			<ul class="f3-widget-paginator">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments36 = array();
// Rendering Boolean node
$arguments36['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext));
$arguments36['then'] = NULL;
$arguments36['else'] = NULL;
$renderChildrenClosure37 = function() use ($renderingContext, $self) {
$output38 = '';

$output38 .= '
					<li class="previous">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments39 = array();
// Rendering Boolean node
$arguments39['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext), 1);
$arguments39['then'] = NULL;
$arguments39['else'] = NULL;
$renderChildrenClosure40 = function() use ($renderingContext, $self) {
$output41 = '';

$output41 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments42 = array();
$renderChildrenClosure43 = function() use ($renderingContext, $self) {
$output44 = '';

$output44 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments45 = array();
// Rendering Array
$array46 = array();
$array46['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments45['arguments'] = $array46;
$arguments45['additionalAttributes'] = NULL;
$arguments45['action'] = NULL;
$arguments45['section'] = '';
$arguments45['format'] = '';
$arguments45['ajax'] = false;
$arguments45['class'] = NULL;
$arguments45['dir'] = NULL;
$arguments45['id'] = NULL;
$arguments45['lang'] = NULL;
$arguments45['style'] = NULL;
$arguments45['title'] = NULL;
$arguments45['accesskey'] = NULL;
$arguments45['tabindex'] = NULL;
$arguments45['onclick'] = NULL;
$arguments45['name'] = NULL;
$arguments45['rel'] = NULL;
$arguments45['rev'] = NULL;
$arguments45['target'] = NULL;
$renderChildrenClosure47 = function() use ($renderingContext, $self) {
$output48 = '';

$output48 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments49 = array();
$arguments49['key'] = 'paginate_previous';
$arguments49['id'] = NULL;
$arguments49['default'] = NULL;
$arguments49['htmlEscape'] = NULL;
$arguments49['arguments'] = NULL;
$arguments49['extensionName'] = NULL;
$renderChildrenClosure50 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper51 = $self->getViewHelper('$viewHelper51', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper51->setArguments($arguments49);
$viewHelper51->setRenderingContext($renderingContext);
$viewHelper51->setRenderChildrenClosure($renderChildrenClosure50);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output48 .= $viewHelper51->initializeArgumentsAndRender();

$output48 .= '
								';
return $output48;
};
$viewHelper52 = $self->getViewHelper('$viewHelper52', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper52->setArguments($arguments45);
$viewHelper52->setRenderingContext($renderingContext);
$viewHelper52->setRenderChildrenClosure($renderChildrenClosure47);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output44 .= $viewHelper52->initializeArgumentsAndRender();

$output44 .= '
							';
return $output44;
};
$viewHelper53 = $self->getViewHelper('$viewHelper53', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper53->setArguments($arguments42);
$viewHelper53->setRenderingContext($renderingContext);
$viewHelper53->setRenderChildrenClosure($renderChildrenClosure43);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output41 .= $viewHelper53->initializeArgumentsAndRender();

$output41 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments54 = array();
$renderChildrenClosure55 = function() use ($renderingContext, $self) {
$output56 = '';

$output56 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments57 = array();
$arguments57['additionalAttributes'] = NULL;
$arguments57['action'] = NULL;
$arguments57['arguments'] = array (
);
$arguments57['section'] = '';
$arguments57['format'] = '';
$arguments57['ajax'] = false;
$arguments57['class'] = NULL;
$arguments57['dir'] = NULL;
$arguments57['id'] = NULL;
$arguments57['lang'] = NULL;
$arguments57['style'] = NULL;
$arguments57['title'] = NULL;
$arguments57['accesskey'] = NULL;
$arguments57['tabindex'] = NULL;
$arguments57['onclick'] = NULL;
$arguments57['name'] = NULL;
$arguments57['rel'] = NULL;
$arguments57['rev'] = NULL;
$arguments57['target'] = NULL;
$renderChildrenClosure58 = function() use ($renderingContext, $self) {
$output59 = '';

$output59 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments60 = array();
$arguments60['key'] = 'paginate_previous';
$arguments60['id'] = NULL;
$arguments60['default'] = NULL;
$arguments60['htmlEscape'] = NULL;
$arguments60['arguments'] = NULL;
$arguments60['extensionName'] = NULL;
$renderChildrenClosure61 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper62 = $self->getViewHelper('$viewHelper62', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper62->setArguments($arguments60);
$viewHelper62->setRenderingContext($renderingContext);
$viewHelper62->setRenderChildrenClosure($renderChildrenClosure61);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output59 .= $viewHelper62->initializeArgumentsAndRender();

$output59 .= '
								';
return $output59;
};
$viewHelper63 = $self->getViewHelper('$viewHelper63', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper63->setArguments($arguments57);
$viewHelper63->setRenderingContext($renderingContext);
$viewHelper63->setRenderChildrenClosure($renderChildrenClosure58);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output56 .= $viewHelper63->initializeArgumentsAndRender();

$output56 .= '
							';
return $output56;
};
$viewHelper64 = $self->getViewHelper('$viewHelper64', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper64->setArguments($arguments54);
$viewHelper64->setRenderingContext($renderingContext);
$viewHelper64->setRenderChildrenClosure($renderChildrenClosure55);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output41 .= $viewHelper64->initializeArgumentsAndRender();

$output41 .= '
						';
return $output41;
};
$arguments39['__thenClosure'] = function() use ($renderingContext, $self) {
$output65 = '';

$output65 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments66 = array();
// Rendering Array
$array67 = array();
$array67['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments66['arguments'] = $array67;
$arguments66['additionalAttributes'] = NULL;
$arguments66['action'] = NULL;
$arguments66['section'] = '';
$arguments66['format'] = '';
$arguments66['ajax'] = false;
$arguments66['class'] = NULL;
$arguments66['dir'] = NULL;
$arguments66['id'] = NULL;
$arguments66['lang'] = NULL;
$arguments66['style'] = NULL;
$arguments66['title'] = NULL;
$arguments66['accesskey'] = NULL;
$arguments66['tabindex'] = NULL;
$arguments66['onclick'] = NULL;
$arguments66['name'] = NULL;
$arguments66['rel'] = NULL;
$arguments66['rev'] = NULL;
$arguments66['target'] = NULL;
$renderChildrenClosure68 = function() use ($renderingContext, $self) {
$output69 = '';

$output69 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments70 = array();
$arguments70['key'] = 'paginate_previous';
$arguments70['id'] = NULL;
$arguments70['default'] = NULL;
$arguments70['htmlEscape'] = NULL;
$arguments70['arguments'] = NULL;
$arguments70['extensionName'] = NULL;
$renderChildrenClosure71 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper72 = $self->getViewHelper('$viewHelper72', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper72->setArguments($arguments70);
$viewHelper72->setRenderingContext($renderingContext);
$viewHelper72->setRenderChildrenClosure($renderChildrenClosure71);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output69 .= $viewHelper72->initializeArgumentsAndRender();

$output69 .= '
								';
return $output69;
};
$viewHelper73 = $self->getViewHelper('$viewHelper73', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper73->setArguments($arguments66);
$viewHelper73->setRenderingContext($renderingContext);
$viewHelper73->setRenderChildrenClosure($renderChildrenClosure68);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output65 .= $viewHelper73->initializeArgumentsAndRender();

$output65 .= '
							';
return $output65;
};
$arguments39['__elseClosure'] = function() use ($renderingContext, $self) {
$output74 = '';

$output74 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments75 = array();
$arguments75['additionalAttributes'] = NULL;
$arguments75['action'] = NULL;
$arguments75['arguments'] = array (
);
$arguments75['section'] = '';
$arguments75['format'] = '';
$arguments75['ajax'] = false;
$arguments75['class'] = NULL;
$arguments75['dir'] = NULL;
$arguments75['id'] = NULL;
$arguments75['lang'] = NULL;
$arguments75['style'] = NULL;
$arguments75['title'] = NULL;
$arguments75['accesskey'] = NULL;
$arguments75['tabindex'] = NULL;
$arguments75['onclick'] = NULL;
$arguments75['name'] = NULL;
$arguments75['rel'] = NULL;
$arguments75['rev'] = NULL;
$arguments75['target'] = NULL;
$renderChildrenClosure76 = function() use ($renderingContext, $self) {
$output77 = '';

$output77 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments78 = array();
$arguments78['key'] = 'paginate_previous';
$arguments78['id'] = NULL;
$arguments78['default'] = NULL;
$arguments78['htmlEscape'] = NULL;
$arguments78['arguments'] = NULL;
$arguments78['extensionName'] = NULL;
$renderChildrenClosure79 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper80 = $self->getViewHelper('$viewHelper80', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper80->setArguments($arguments78);
$viewHelper80->setRenderingContext($renderingContext);
$viewHelper80->setRenderChildrenClosure($renderChildrenClosure79);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output77 .= $viewHelper80->initializeArgumentsAndRender();

$output77 .= '
								';
return $output77;
};
$viewHelper81 = $self->getViewHelper('$viewHelper81', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper81->setArguments($arguments75);
$viewHelper81->setRenderingContext($renderingContext);
$viewHelper81->setRenderChildrenClosure($renderChildrenClosure76);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output74 .= $viewHelper81->initializeArgumentsAndRender();

$output74 .= '
							';
return $output74;
};
$viewHelper82 = $self->getViewHelper('$viewHelper82', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper82->setArguments($arguments39);
$viewHelper82->setRenderingContext($renderingContext);
$viewHelper82->setRenderChildrenClosure($renderChildrenClosure40);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output38 .= $viewHelper82->initializeArgumentsAndRender();

$output38 .= '
					</li>
				';
return $output38;
};
$viewHelper83 = $self->getViewHelper('$viewHelper83', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper83->setArguments($arguments36);
$viewHelper83->setRenderingContext($renderingContext);
$viewHelper83->setRenderChildrenClosure($renderChildrenClosure37);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper83->initializeArgumentsAndRender();

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments84 = array();
// Rendering Boolean node
$arguments84['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.displayRangeStart', $renderingContext), 1);
$arguments84['then'] = NULL;
$arguments84['else'] = NULL;
$renderChildrenClosure85 = function() use ($renderingContext, $self) {
$output86 = '';

$output86 .= '
					<li class="first">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments87 = array();
$arguments87['additionalAttributes'] = NULL;
$arguments87['action'] = NULL;
$arguments87['arguments'] = array (
);
$arguments87['section'] = '';
$arguments87['format'] = '';
$arguments87['ajax'] = false;
$arguments87['class'] = NULL;
$arguments87['dir'] = NULL;
$arguments87['id'] = NULL;
$arguments87['lang'] = NULL;
$arguments87['style'] = NULL;
$arguments87['title'] = NULL;
$arguments87['accesskey'] = NULL;
$arguments87['tabindex'] = NULL;
$arguments87['onclick'] = NULL;
$arguments87['name'] = NULL;
$arguments87['rel'] = NULL;
$arguments87['rev'] = NULL;
$arguments87['target'] = NULL;
$renderChildrenClosure88 = function() use ($renderingContext, $self) {
return '1';
};
$viewHelper89 = $self->getViewHelper('$viewHelper89', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper89->setArguments($arguments87);
$viewHelper89->setRenderingContext($renderingContext);
$viewHelper89->setRenderChildrenClosure($renderChildrenClosure88);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output86 .= $viewHelper89->initializeArgumentsAndRender();

$output86 .= '
					</li>
				';
return $output86;
};
$viewHelper90 = $self->getViewHelper('$viewHelper90', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper90->setArguments($arguments84);
$viewHelper90->setRenderingContext($renderingContext);
$viewHelper90->setRenderChildrenClosure($renderChildrenClosure85);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper90->initializeArgumentsAndRender();

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments91 = array();
// Rendering Boolean node
$arguments91['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.hasLessPages', $renderingContext));
$arguments91['then'] = NULL;
$arguments91['else'] = NULL;
$renderChildrenClosure92 = function() use ($renderingContext, $self) {
return '
					<li>....</li>
				';
};
$viewHelper93 = $self->getViewHelper('$viewHelper93', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper93->setArguments($arguments91);
$viewHelper93->setRenderingContext($renderingContext);
$viewHelper93->setRenderChildrenClosure($renderChildrenClosure92);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper93->initializeArgumentsAndRender();

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments94 = array();
$arguments94['each'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.pages', $renderingContext);
$arguments94['as'] = 'page';
$arguments94['key'] = '';
$arguments94['reverse'] = false;
$arguments94['iteration'] = NULL;
$renderChildrenClosure95 = function() use ($renderingContext, $self) {
$output96 = '';

$output96 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments97 = array();
// Rendering Boolean node
$arguments97['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.isCurrent', $renderingContext));
$arguments97['then'] = NULL;
$arguments97['else'] = NULL;
$renderChildrenClosure98 = function() use ($renderingContext, $self) {
$output99 = '';

$output99 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments100 = array();
$renderChildrenClosure101 = function() use ($renderingContext, $self) {
$output102 = '';

$output102 .= '
							<li class="current">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments103 = array();
$arguments103['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments103['keepQuotes'] = false;
$arguments103['encoding'] = NULL;
$arguments103['doubleEncode'] = true;
$renderChildrenClosure104 = function() use ($renderingContext, $self) {
return NULL;
};
$value105 = ($arguments103['value'] !== NULL ? $arguments103['value'] : $renderChildrenClosure104());

$output102 .= (!is_string($value105) ? $value105 : htmlspecialchars($value105, ($arguments103['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments103['encoding'] !== NULL ? $arguments103['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments103['doubleEncode']));

$output102 .= '</li>
						';
return $output102;
};
$viewHelper106 = $self->getViewHelper('$viewHelper106', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper106->setArguments($arguments100);
$viewHelper106->setRenderingContext($renderingContext);
$viewHelper106->setRenderChildrenClosure($renderChildrenClosure101);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output99 .= $viewHelper106->initializeArgumentsAndRender();

$output99 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments107 = array();
$renderChildrenClosure108 = function() use ($renderingContext, $self) {
$output109 = '';

$output109 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments110 = array();
// Rendering Boolean node
$arguments110['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext), 1);
$arguments110['then'] = NULL;
$arguments110['else'] = NULL;
$renderChildrenClosure111 = function() use ($renderingContext, $self) {
$output112 = '';

$output112 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments113 = array();
$renderChildrenClosure114 = function() use ($renderingContext, $self) {
$output115 = '';

$output115 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments116 = array();
// Rendering Array
$array117 = array();
$array117['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments116['arguments'] = $array117;
$arguments116['additionalAttributes'] = NULL;
$arguments116['action'] = NULL;
$arguments116['section'] = '';
$arguments116['format'] = '';
$arguments116['ajax'] = false;
$arguments116['class'] = NULL;
$arguments116['dir'] = NULL;
$arguments116['id'] = NULL;
$arguments116['lang'] = NULL;
$arguments116['style'] = NULL;
$arguments116['title'] = NULL;
$arguments116['accesskey'] = NULL;
$arguments116['tabindex'] = NULL;
$arguments116['onclick'] = NULL;
$arguments116['name'] = NULL;
$arguments116['rel'] = NULL;
$arguments116['rev'] = NULL;
$arguments116['target'] = NULL;
$renderChildrenClosure118 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments119 = array();
$arguments119['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments119['keepQuotes'] = false;
$arguments119['encoding'] = NULL;
$arguments119['doubleEncode'] = true;
$renderChildrenClosure120 = function() use ($renderingContext, $self) {
return NULL;
};
$value121 = ($arguments119['value'] !== NULL ? $arguments119['value'] : $renderChildrenClosure120());
return (!is_string($value121) ? $value121 : htmlspecialchars($value121, ($arguments119['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments119['encoding'] !== NULL ? $arguments119['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments119['doubleEncode']));
};
$viewHelper122 = $self->getViewHelper('$viewHelper122', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper122->setArguments($arguments116);
$viewHelper122->setRenderingContext($renderingContext);
$viewHelper122->setRenderChildrenClosure($renderChildrenClosure118);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output115 .= $viewHelper122->initializeArgumentsAndRender();

$output115 .= '
									';
return $output115;
};
$viewHelper123 = $self->getViewHelper('$viewHelper123', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper123->setArguments($arguments113);
$viewHelper123->setRenderingContext($renderingContext);
$viewHelper123->setRenderChildrenClosure($renderChildrenClosure114);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output112 .= $viewHelper123->initializeArgumentsAndRender();

$output112 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments124 = array();
$renderChildrenClosure125 = function() use ($renderingContext, $self) {
$output126 = '';

$output126 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments127 = array();
$arguments127['additionalAttributes'] = NULL;
$arguments127['action'] = NULL;
$arguments127['arguments'] = array (
);
$arguments127['section'] = '';
$arguments127['format'] = '';
$arguments127['ajax'] = false;
$arguments127['class'] = NULL;
$arguments127['dir'] = NULL;
$arguments127['id'] = NULL;
$arguments127['lang'] = NULL;
$arguments127['style'] = NULL;
$arguments127['title'] = NULL;
$arguments127['accesskey'] = NULL;
$arguments127['tabindex'] = NULL;
$arguments127['onclick'] = NULL;
$arguments127['name'] = NULL;
$arguments127['rel'] = NULL;
$arguments127['rev'] = NULL;
$arguments127['target'] = NULL;
$renderChildrenClosure128 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments129 = array();
$arguments129['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments129['keepQuotes'] = false;
$arguments129['encoding'] = NULL;
$arguments129['doubleEncode'] = true;
$renderChildrenClosure130 = function() use ($renderingContext, $self) {
return NULL;
};
$value131 = ($arguments129['value'] !== NULL ? $arguments129['value'] : $renderChildrenClosure130());
return (!is_string($value131) ? $value131 : htmlspecialchars($value131, ($arguments129['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments129['encoding'] !== NULL ? $arguments129['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments129['doubleEncode']));
};
$viewHelper132 = $self->getViewHelper('$viewHelper132', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper132->setArguments($arguments127);
$viewHelper132->setRenderingContext($renderingContext);
$viewHelper132->setRenderChildrenClosure($renderChildrenClosure128);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output126 .= $viewHelper132->initializeArgumentsAndRender();

$output126 .= '
									';
return $output126;
};
$viewHelper133 = $self->getViewHelper('$viewHelper133', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper133->setArguments($arguments124);
$viewHelper133->setRenderingContext($renderingContext);
$viewHelper133->setRenderChildrenClosure($renderChildrenClosure125);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output112 .= $viewHelper133->initializeArgumentsAndRender();

$output112 .= '
								';
return $output112;
};
$arguments110['__thenClosure'] = function() use ($renderingContext, $self) {
$output134 = '';

$output134 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments135 = array();
// Rendering Array
$array136 = array();
$array136['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments135['arguments'] = $array136;
$arguments135['additionalAttributes'] = NULL;
$arguments135['action'] = NULL;
$arguments135['section'] = '';
$arguments135['format'] = '';
$arguments135['ajax'] = false;
$arguments135['class'] = NULL;
$arguments135['dir'] = NULL;
$arguments135['id'] = NULL;
$arguments135['lang'] = NULL;
$arguments135['style'] = NULL;
$arguments135['title'] = NULL;
$arguments135['accesskey'] = NULL;
$arguments135['tabindex'] = NULL;
$arguments135['onclick'] = NULL;
$arguments135['name'] = NULL;
$arguments135['rel'] = NULL;
$arguments135['rev'] = NULL;
$arguments135['target'] = NULL;
$renderChildrenClosure137 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments138 = array();
$arguments138['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments138['keepQuotes'] = false;
$arguments138['encoding'] = NULL;
$arguments138['doubleEncode'] = true;
$renderChildrenClosure139 = function() use ($renderingContext, $self) {
return NULL;
};
$value140 = ($arguments138['value'] !== NULL ? $arguments138['value'] : $renderChildrenClosure139());
return (!is_string($value140) ? $value140 : htmlspecialchars($value140, ($arguments138['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments138['encoding'] !== NULL ? $arguments138['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments138['doubleEncode']));
};
$viewHelper141 = $self->getViewHelper('$viewHelper141', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper141->setArguments($arguments135);
$viewHelper141->setRenderingContext($renderingContext);
$viewHelper141->setRenderChildrenClosure($renderChildrenClosure137);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output134 .= $viewHelper141->initializeArgumentsAndRender();

$output134 .= '
									';
return $output134;
};
$arguments110['__elseClosure'] = function() use ($renderingContext, $self) {
$output142 = '';

$output142 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments143 = array();
$arguments143['additionalAttributes'] = NULL;
$arguments143['action'] = NULL;
$arguments143['arguments'] = array (
);
$arguments143['section'] = '';
$arguments143['format'] = '';
$arguments143['ajax'] = false;
$arguments143['class'] = NULL;
$arguments143['dir'] = NULL;
$arguments143['id'] = NULL;
$arguments143['lang'] = NULL;
$arguments143['style'] = NULL;
$arguments143['title'] = NULL;
$arguments143['accesskey'] = NULL;
$arguments143['tabindex'] = NULL;
$arguments143['onclick'] = NULL;
$arguments143['name'] = NULL;
$arguments143['rel'] = NULL;
$arguments143['rev'] = NULL;
$arguments143['target'] = NULL;
$renderChildrenClosure144 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments145 = array();
$arguments145['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments145['keepQuotes'] = false;
$arguments145['encoding'] = NULL;
$arguments145['doubleEncode'] = true;
$renderChildrenClosure146 = function() use ($renderingContext, $self) {
return NULL;
};
$value147 = ($arguments145['value'] !== NULL ? $arguments145['value'] : $renderChildrenClosure146());
return (!is_string($value147) ? $value147 : htmlspecialchars($value147, ($arguments145['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments145['encoding'] !== NULL ? $arguments145['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments145['doubleEncode']));
};
$viewHelper148 = $self->getViewHelper('$viewHelper148', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper148->setArguments($arguments143);
$viewHelper148->setRenderingContext($renderingContext);
$viewHelper148->setRenderChildrenClosure($renderChildrenClosure144);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output142 .= $viewHelper148->initializeArgumentsAndRender();

$output142 .= '
									';
return $output142;
};
$viewHelper149 = $self->getViewHelper('$viewHelper149', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper149->setArguments($arguments110);
$viewHelper149->setRenderingContext($renderingContext);
$viewHelper149->setRenderChildrenClosure($renderChildrenClosure111);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output109 .= $viewHelper149->initializeArgumentsAndRender();

$output109 .= '
							</li>
						';
return $output109;
};
$viewHelper150 = $self->getViewHelper('$viewHelper150', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper150->setArguments($arguments107);
$viewHelper150->setRenderingContext($renderingContext);
$viewHelper150->setRenderChildrenClosure($renderChildrenClosure108);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output99 .= $viewHelper150->initializeArgumentsAndRender();

$output99 .= '
					';
return $output99;
};
$arguments97['__thenClosure'] = function() use ($renderingContext, $self) {
$output151 = '';

$output151 .= '
							<li class="current">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments152 = array();
$arguments152['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments152['keepQuotes'] = false;
$arguments152['encoding'] = NULL;
$arguments152['doubleEncode'] = true;
$renderChildrenClosure153 = function() use ($renderingContext, $self) {
return NULL;
};
$value154 = ($arguments152['value'] !== NULL ? $arguments152['value'] : $renderChildrenClosure153());

$output151 .= (!is_string($value154) ? $value154 : htmlspecialchars($value154, ($arguments152['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments152['encoding'] !== NULL ? $arguments152['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments152['doubleEncode']));

$output151 .= '</li>
						';
return $output151;
};
$arguments97['__elseClosure'] = function() use ($renderingContext, $self) {
$output155 = '';

$output155 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments156 = array();
// Rendering Boolean node
$arguments156['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext), 1);
$arguments156['then'] = NULL;
$arguments156['else'] = NULL;
$renderChildrenClosure157 = function() use ($renderingContext, $self) {
$output158 = '';

$output158 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments159 = array();
$renderChildrenClosure160 = function() use ($renderingContext, $self) {
$output161 = '';

$output161 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments162 = array();
// Rendering Array
$array163 = array();
$array163['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments162['arguments'] = $array163;
$arguments162['additionalAttributes'] = NULL;
$arguments162['action'] = NULL;
$arguments162['section'] = '';
$arguments162['format'] = '';
$arguments162['ajax'] = false;
$arguments162['class'] = NULL;
$arguments162['dir'] = NULL;
$arguments162['id'] = NULL;
$arguments162['lang'] = NULL;
$arguments162['style'] = NULL;
$arguments162['title'] = NULL;
$arguments162['accesskey'] = NULL;
$arguments162['tabindex'] = NULL;
$arguments162['onclick'] = NULL;
$arguments162['name'] = NULL;
$arguments162['rel'] = NULL;
$arguments162['rev'] = NULL;
$arguments162['target'] = NULL;
$renderChildrenClosure164 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments165 = array();
$arguments165['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments165['keepQuotes'] = false;
$arguments165['encoding'] = NULL;
$arguments165['doubleEncode'] = true;
$renderChildrenClosure166 = function() use ($renderingContext, $self) {
return NULL;
};
$value167 = ($arguments165['value'] !== NULL ? $arguments165['value'] : $renderChildrenClosure166());
return (!is_string($value167) ? $value167 : htmlspecialchars($value167, ($arguments165['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments165['encoding'] !== NULL ? $arguments165['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments165['doubleEncode']));
};
$viewHelper168 = $self->getViewHelper('$viewHelper168', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper168->setArguments($arguments162);
$viewHelper168->setRenderingContext($renderingContext);
$viewHelper168->setRenderChildrenClosure($renderChildrenClosure164);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output161 .= $viewHelper168->initializeArgumentsAndRender();

$output161 .= '
									';
return $output161;
};
$viewHelper169 = $self->getViewHelper('$viewHelper169', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper169->setArguments($arguments159);
$viewHelper169->setRenderingContext($renderingContext);
$viewHelper169->setRenderChildrenClosure($renderChildrenClosure160);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output158 .= $viewHelper169->initializeArgumentsAndRender();

$output158 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments170 = array();
$renderChildrenClosure171 = function() use ($renderingContext, $self) {
$output172 = '';

$output172 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments173 = array();
$arguments173['additionalAttributes'] = NULL;
$arguments173['action'] = NULL;
$arguments173['arguments'] = array (
);
$arguments173['section'] = '';
$arguments173['format'] = '';
$arguments173['ajax'] = false;
$arguments173['class'] = NULL;
$arguments173['dir'] = NULL;
$arguments173['id'] = NULL;
$arguments173['lang'] = NULL;
$arguments173['style'] = NULL;
$arguments173['title'] = NULL;
$arguments173['accesskey'] = NULL;
$arguments173['tabindex'] = NULL;
$arguments173['onclick'] = NULL;
$arguments173['name'] = NULL;
$arguments173['rel'] = NULL;
$arguments173['rev'] = NULL;
$arguments173['target'] = NULL;
$renderChildrenClosure174 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments175 = array();
$arguments175['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments175['keepQuotes'] = false;
$arguments175['encoding'] = NULL;
$arguments175['doubleEncode'] = true;
$renderChildrenClosure176 = function() use ($renderingContext, $self) {
return NULL;
};
$value177 = ($arguments175['value'] !== NULL ? $arguments175['value'] : $renderChildrenClosure176());
return (!is_string($value177) ? $value177 : htmlspecialchars($value177, ($arguments175['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments175['encoding'] !== NULL ? $arguments175['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments175['doubleEncode']));
};
$viewHelper178 = $self->getViewHelper('$viewHelper178', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper178->setArguments($arguments173);
$viewHelper178->setRenderingContext($renderingContext);
$viewHelper178->setRenderChildrenClosure($renderChildrenClosure174);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output172 .= $viewHelper178->initializeArgumentsAndRender();

$output172 .= '
									';
return $output172;
};
$viewHelper179 = $self->getViewHelper('$viewHelper179', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper179->setArguments($arguments170);
$viewHelper179->setRenderingContext($renderingContext);
$viewHelper179->setRenderChildrenClosure($renderChildrenClosure171);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output158 .= $viewHelper179->initializeArgumentsAndRender();

$output158 .= '
								';
return $output158;
};
$arguments156['__thenClosure'] = function() use ($renderingContext, $self) {
$output180 = '';

$output180 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments181 = array();
// Rendering Array
$array182 = array();
$array182['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments181['arguments'] = $array182;
$arguments181['additionalAttributes'] = NULL;
$arguments181['action'] = NULL;
$arguments181['section'] = '';
$arguments181['format'] = '';
$arguments181['ajax'] = false;
$arguments181['class'] = NULL;
$arguments181['dir'] = NULL;
$arguments181['id'] = NULL;
$arguments181['lang'] = NULL;
$arguments181['style'] = NULL;
$arguments181['title'] = NULL;
$arguments181['accesskey'] = NULL;
$arguments181['tabindex'] = NULL;
$arguments181['onclick'] = NULL;
$arguments181['name'] = NULL;
$arguments181['rel'] = NULL;
$arguments181['rev'] = NULL;
$arguments181['target'] = NULL;
$renderChildrenClosure183 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments184 = array();
$arguments184['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments184['keepQuotes'] = false;
$arguments184['encoding'] = NULL;
$arguments184['doubleEncode'] = true;
$renderChildrenClosure185 = function() use ($renderingContext, $self) {
return NULL;
};
$value186 = ($arguments184['value'] !== NULL ? $arguments184['value'] : $renderChildrenClosure185());
return (!is_string($value186) ? $value186 : htmlspecialchars($value186, ($arguments184['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments184['encoding'] !== NULL ? $arguments184['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments184['doubleEncode']));
};
$viewHelper187 = $self->getViewHelper('$viewHelper187', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper187->setArguments($arguments181);
$viewHelper187->setRenderingContext($renderingContext);
$viewHelper187->setRenderChildrenClosure($renderChildrenClosure183);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output180 .= $viewHelper187->initializeArgumentsAndRender();

$output180 .= '
									';
return $output180;
};
$arguments156['__elseClosure'] = function() use ($renderingContext, $self) {
$output188 = '';

$output188 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments189 = array();
$arguments189['additionalAttributes'] = NULL;
$arguments189['action'] = NULL;
$arguments189['arguments'] = array (
);
$arguments189['section'] = '';
$arguments189['format'] = '';
$arguments189['ajax'] = false;
$arguments189['class'] = NULL;
$arguments189['dir'] = NULL;
$arguments189['id'] = NULL;
$arguments189['lang'] = NULL;
$arguments189['style'] = NULL;
$arguments189['title'] = NULL;
$arguments189['accesskey'] = NULL;
$arguments189['tabindex'] = NULL;
$arguments189['onclick'] = NULL;
$arguments189['name'] = NULL;
$arguments189['rel'] = NULL;
$arguments189['rev'] = NULL;
$arguments189['target'] = NULL;
$renderChildrenClosure190 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments191 = array();
$arguments191['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments191['keepQuotes'] = false;
$arguments191['encoding'] = NULL;
$arguments191['doubleEncode'] = true;
$renderChildrenClosure192 = function() use ($renderingContext, $self) {
return NULL;
};
$value193 = ($arguments191['value'] !== NULL ? $arguments191['value'] : $renderChildrenClosure192());
return (!is_string($value193) ? $value193 : htmlspecialchars($value193, ($arguments191['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments191['encoding'] !== NULL ? $arguments191['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments191['doubleEncode']));
};
$viewHelper194 = $self->getViewHelper('$viewHelper194', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper194->setArguments($arguments189);
$viewHelper194->setRenderingContext($renderingContext);
$viewHelper194->setRenderChildrenClosure($renderChildrenClosure190);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output188 .= $viewHelper194->initializeArgumentsAndRender();

$output188 .= '
									';
return $output188;
};
$viewHelper195 = $self->getViewHelper('$viewHelper195', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper195->setArguments($arguments156);
$viewHelper195->setRenderingContext($renderingContext);
$viewHelper195->setRenderChildrenClosure($renderChildrenClosure157);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output155 .= $viewHelper195->initializeArgumentsAndRender();

$output155 .= '
							</li>
						';
return $output155;
};
$viewHelper196 = $self->getViewHelper('$viewHelper196', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper196->setArguments($arguments97);
$viewHelper196->setRenderingContext($renderingContext);
$viewHelper196->setRenderChildrenClosure($renderChildrenClosure98);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output96 .= $viewHelper196->initializeArgumentsAndRender();

$output96 .= '
				';
return $output96;
};

$output3 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments94, $renderChildrenClosure95, $renderingContext);

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments197 = array();
// Rendering Boolean node
$arguments197['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.hasMorePages', $renderingContext));
$arguments197['then'] = NULL;
$arguments197['else'] = NULL;
$renderChildrenClosure198 = function() use ($renderingContext, $self) {
return '
					<li>....</li>
				';
};
$viewHelper199 = $self->getViewHelper('$viewHelper199', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper199->setArguments($arguments197);
$viewHelper199->setRenderingContext($renderingContext);
$viewHelper199->setRenderChildrenClosure($renderChildrenClosure198);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper199->initializeArgumentsAndRender();

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments200 = array();
// Rendering Boolean node
$arguments200['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.displayRangeEnd', $renderingContext), TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext));
$arguments200['then'] = NULL;
$arguments200['else'] = NULL;
$renderChildrenClosure201 = function() use ($renderingContext, $self) {
$output202 = '';

$output202 .= '
					<li class="last">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments203 = array();
// Rendering Array
$array204 = array();
$array204['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments203['arguments'] = $array204;
$arguments203['additionalAttributes'] = NULL;
$arguments203['action'] = NULL;
$arguments203['section'] = '';
$arguments203['format'] = '';
$arguments203['ajax'] = false;
$arguments203['class'] = NULL;
$arguments203['dir'] = NULL;
$arguments203['id'] = NULL;
$arguments203['lang'] = NULL;
$arguments203['style'] = NULL;
$arguments203['title'] = NULL;
$arguments203['accesskey'] = NULL;
$arguments203['tabindex'] = NULL;
$arguments203['onclick'] = NULL;
$arguments203['name'] = NULL;
$arguments203['rel'] = NULL;
$arguments203['rev'] = NULL;
$arguments203['target'] = NULL;
$renderChildrenClosure205 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments206 = array();
$arguments206['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments206['keepQuotes'] = false;
$arguments206['encoding'] = NULL;
$arguments206['doubleEncode'] = true;
$renderChildrenClosure207 = function() use ($renderingContext, $self) {
return NULL;
};
$value208 = ($arguments206['value'] !== NULL ? $arguments206['value'] : $renderChildrenClosure207());
return (!is_string($value208) ? $value208 : htmlspecialchars($value208, ($arguments206['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments206['encoding'] !== NULL ? $arguments206['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments206['doubleEncode']));
};
$viewHelper209 = $self->getViewHelper('$viewHelper209', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper209->setArguments($arguments203);
$viewHelper209->setRenderingContext($renderingContext);
$viewHelper209->setRenderChildrenClosure($renderChildrenClosure205);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output202 .= $viewHelper209->initializeArgumentsAndRender();

$output202 .= '
					</li>
				';
return $output202;
};
$viewHelper210 = $self->getViewHelper('$viewHelper210', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper210->setArguments($arguments200);
$viewHelper210->setRenderingContext($renderingContext);
$viewHelper210->setRenderChildrenClosure($renderChildrenClosure201);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper210->initializeArgumentsAndRender();

$output3 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments211 = array();
// Rendering Boolean node
$arguments211['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext));
$arguments211['then'] = NULL;
$arguments211['else'] = NULL;
$renderChildrenClosure212 = function() use ($renderingContext, $self) {
$output213 = '';

$output213 .= '
					<li class="last next">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments214 = array();
// Rendering Array
$array215 = array();
$array215['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext);
$arguments214['arguments'] = $array215;
$arguments214['additionalAttributes'] = NULL;
$arguments214['action'] = NULL;
$arguments214['section'] = '';
$arguments214['format'] = '';
$arguments214['ajax'] = false;
$arguments214['class'] = NULL;
$arguments214['dir'] = NULL;
$arguments214['id'] = NULL;
$arguments214['lang'] = NULL;
$arguments214['style'] = NULL;
$arguments214['title'] = NULL;
$arguments214['accesskey'] = NULL;
$arguments214['tabindex'] = NULL;
$arguments214['onclick'] = NULL;
$arguments214['name'] = NULL;
$arguments214['rel'] = NULL;
$arguments214['rev'] = NULL;
$arguments214['target'] = NULL;
$renderChildrenClosure216 = function() use ($renderingContext, $self) {
$output217 = '';

$output217 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments218 = array();
$arguments218['key'] = 'paginate_next';
$arguments218['id'] = NULL;
$arguments218['default'] = NULL;
$arguments218['htmlEscape'] = NULL;
$arguments218['arguments'] = NULL;
$arguments218['extensionName'] = NULL;
$renderChildrenClosure219 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper220 = $self->getViewHelper('$viewHelper220', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper220->setArguments($arguments218);
$viewHelper220->setRenderingContext($renderingContext);
$viewHelper220->setRenderChildrenClosure($renderChildrenClosure219);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output217 .= $viewHelper220->initializeArgumentsAndRender();

$output217 .= '
						';
return $output217;
};
$viewHelper221 = $self->getViewHelper('$viewHelper221', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper221->setArguments($arguments214);
$viewHelper221->setRenderingContext($renderingContext);
$viewHelper221->setRenderChildrenClosure($renderChildrenClosure216);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output213 .= $viewHelper221->initializeArgumentsAndRender();

$output213 .= '
					</li>
				';
return $output213;
};
$viewHelper222 = $self->getViewHelper('$viewHelper222', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper222->setArguments($arguments211);
$viewHelper222->setRenderingContext($renderingContext);
$viewHelper222->setRenderChildrenClosure($renderChildrenClosure212);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output3 .= $viewHelper222->initializeArgumentsAndRender();

$output3 .= '
			</ul>
		</div>
		<div class="news-clear"></div>
	';
return $output3;
};
$viewHelper223 = $self->getViewHelper('$viewHelper223', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper223->setArguments($arguments1);
$viewHelper223->setRenderingContext($renderingContext);
$viewHelper223->setRenderChildrenClosure($renderChildrenClosure2);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output0 .= $viewHelper223->initializeArgumentsAndRender();

$output0 .= '
';

return $output0;
}
/**
 * Main Render function
 */
public function render(\TYPO3\CMS\Fluid\Core\Rendering\RenderingContextInterface $renderingContext) {
$self = $this;
$output224 = '';

$output224 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments225 = array();
// Rendering Boolean node
$arguments225['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.insertAbove', $renderingContext));
$arguments225['then'] = NULL;
$arguments225['else'] = NULL;
$renderChildrenClosure226 = function() use ($renderingContext, $self) {
$output227 = '';

$output227 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments228 = array();
$arguments228['section'] = 'paginator';
// Rendering Array
$array229 = array();
$array229['pagination'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination', $renderingContext);
$array229['configuration'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration', $renderingContext);
$arguments228['arguments'] = $array229;
$arguments228['partial'] = NULL;
$arguments228['optional'] = false;
$renderChildrenClosure230 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper231 = $self->getViewHelper('$viewHelper231', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper231->setArguments($arguments228);
$viewHelper231->setRenderingContext($renderingContext);
$viewHelper231->setRenderChildrenClosure($renderChildrenClosure230);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output227 .= $viewHelper231->initializeArgumentsAndRender();

$output227 .= '
';
return $output227;
};
$viewHelper232 = $self->getViewHelper('$viewHelper232', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper232->setArguments($arguments225);
$viewHelper232->setRenderingContext($renderingContext);
$viewHelper232->setRenderChildrenClosure($renderChildrenClosure226);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output224 .= $viewHelper232->initializeArgumentsAndRender();

$output224 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper
$arguments233 = array();
$arguments233['arguments'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'contentArguments', $renderingContext);
$renderChildrenClosure234 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper235 = $self->getViewHelper('$viewHelper235', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper');
$viewHelper235->setArguments($arguments233);
$viewHelper235->setRenderingContext($renderingContext);
$viewHelper235->setRenderChildrenClosure($renderChildrenClosure234);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderChildrenViewHelper

$output224 .= $viewHelper235->initializeArgumentsAndRender();

$output224 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments236 = array();
// Rendering Boolean node
$arguments236['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration.insertBelow', $renderingContext));
$arguments236['then'] = NULL;
$arguments236['else'] = NULL;
$renderChildrenClosure237 = function() use ($renderingContext, $self) {
$output238 = '';

$output238 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
$arguments239 = array();
$arguments239['section'] = 'paginator';
// Rendering Array
$array240 = array();
$array240['pagination'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination', $renderingContext);
$array240['configuration'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'configuration', $renderingContext);
$arguments239['arguments'] = $array240;
$arguments239['partial'] = NULL;
$arguments239['optional'] = false;
$renderChildrenClosure241 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper242 = $self->getViewHelper('$viewHelper242', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper');
$viewHelper242->setArguments($arguments239);
$viewHelper242->setRenderingContext($renderingContext);
$viewHelper242->setRenderChildrenClosure($renderChildrenClosure241);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper

$output238 .= $viewHelper242->initializeArgumentsAndRender();

$output238 .= '
';
return $output238;
};
$viewHelper243 = $self->getViewHelper('$viewHelper243', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper243->setArguments($arguments236);
$viewHelper243->setRenderingContext($renderingContext);
$viewHelper243->setRenderChildrenClosure($renderChildrenClosure237);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output224 .= $viewHelper243->initializeArgumentsAndRender();

$output224 .= '

';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\SectionViewHelper
$arguments244 = array();
$arguments244['name'] = 'paginator';
$renderChildrenClosure245 = function() use ($renderingContext, $self) {
$output246 = '';

$output246 .= '
	';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments247 = array();
// Rendering Boolean node
$arguments247['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext), 1);
$arguments247['then'] = NULL;
$arguments247['else'] = NULL;
$renderChildrenClosure248 = function() use ($renderingContext, $self) {
$output249 = '';

$output249 .= '
		';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments250 = array();
// Rendering Boolean node
$arguments250['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'settings.list.paginate', $renderingContext));
$arguments250['then'] = NULL;
$arguments250['else'] = NULL;
$renderChildrenClosure251 = function() use ($renderingContext, $self) {
$output252 = '';

$output252 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments253 = array();
// Rendering Boolean node
$arguments253['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.current', $renderingContext), 1);
$arguments253['then'] = NULL;
$arguments253['else'] = NULL;
$renderChildrenClosure254 = function() use ($renderingContext, $self) {
$output255 = '';

$output255 .= '
				';
// Rendering ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper
$arguments256 = array();
$renderChildrenClosure257 = function() use ($renderingContext, $self) {
$output258 = '';

$output258 .= '<link rel="prev" href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper
$arguments259 = array();
// Rendering Array
$array260 = array();
$array260['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments259['arguments'] = $array260;
$arguments259['action'] = NULL;
$arguments259['section'] = '';
$arguments259['format'] = '';
$arguments259['ajax'] = false;
$renderChildrenClosure261 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper262 = $self->getViewHelper('$viewHelper262', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper');
$viewHelper262->setArguments($arguments259);
$viewHelper262->setRenderingContext($renderingContext);
$viewHelper262->setRenderChildrenClosure($renderChildrenClosure261);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper

$output258 .= $viewHelper262->initializeArgumentsAndRender();

$output258 .= '" />';
return $output258;
};
$viewHelper263 = $self->getViewHelper('$viewHelper263', $renderingContext, 'Tx_News_ViewHelpers_HeaderDataViewHelper');
$viewHelper263->setArguments($arguments256);
$viewHelper263->setRenderingContext($renderingContext);
$viewHelper263->setRenderChildrenClosure($renderChildrenClosure257);
// End of ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper

$output255 .= $viewHelper263->initializeArgumentsAndRender();

$output255 .= '
			';
return $output255;
};
$viewHelper264 = $self->getViewHelper('$viewHelper264', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper264->setArguments($arguments253);
$viewHelper264->setRenderingContext($renderingContext);
$viewHelper264->setRenderChildrenClosure($renderChildrenClosure254);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper264->initializeArgumentsAndRender();

$output252 .= '
			';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments265 = array();
// Rendering Boolean node
$arguments265['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext));
$arguments265['then'] = NULL;
$arguments265['else'] = NULL;
$renderChildrenClosure266 = function() use ($renderingContext, $self) {
$output267 = '';

$output267 .= '
				';
// Rendering ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper
$arguments268 = array();
$renderChildrenClosure269 = function() use ($renderingContext, $self) {
$output270 = '';

$output270 .= '<link rel="next" href="';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper
$arguments271 = array();
// Rendering Array
$array272 = array();
$array272['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext);
$arguments271['arguments'] = $array272;
$arguments271['action'] = NULL;
$arguments271['section'] = '';
$arguments271['format'] = '';
$arguments271['ajax'] = false;
$renderChildrenClosure273 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper274 = $self->getViewHelper('$viewHelper274', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper');
$viewHelper274->setArguments($arguments271);
$viewHelper274->setRenderingContext($renderingContext);
$viewHelper274->setRenderChildrenClosure($renderChildrenClosure273);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\UriViewHelper

$output270 .= $viewHelper274->initializeArgumentsAndRender();

$output270 .= '" />';
return $output270;
};
$viewHelper275 = $self->getViewHelper('$viewHelper275', $renderingContext, 'Tx_News_ViewHelpers_HeaderDataViewHelper');
$viewHelper275->setArguments($arguments268);
$viewHelper275->setRenderingContext($renderingContext);
$viewHelper275->setRenderChildrenClosure($renderChildrenClosure269);
// End of ViewHelper Tx_News_ViewHelpers_HeaderDataViewHelper

$output267 .= $viewHelper275->initializeArgumentsAndRender();

$output267 .= '
			';
return $output267;
};
$viewHelper276 = $self->getViewHelper('$viewHelper276', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper276->setArguments($arguments265);
$viewHelper276->setRenderingContext($renderingContext);
$viewHelper276->setRenderChildrenClosure($renderChildrenClosure266);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output252 .= $viewHelper276->initializeArgumentsAndRender();

$output252 .= '
		';
return $output252;
};
$viewHelper277 = $self->getViewHelper('$viewHelper277', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper277->setArguments($arguments250);
$viewHelper277->setRenderingContext($renderingContext);
$viewHelper277->setRenderChildrenClosure($renderChildrenClosure251);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper277->initializeArgumentsAndRender();

$output249 .= '

		<div class="page-navigation">
			<p>
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments278 = array();
$arguments278['key'] = 'paginate_overall';
// Rendering Array
$array279 = array();
$array279['0'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.current', $renderingContext);
$array279['1'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments278['arguments'] = $array279;
$arguments278['id'] = NULL;
$arguments278['default'] = NULL;
$arguments278['htmlEscape'] = NULL;
$arguments278['extensionName'] = NULL;
$renderChildrenClosure280 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper281 = $self->getViewHelper('$viewHelper281', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper281->setArguments($arguments278);
$viewHelper281->setRenderingContext($renderingContext);
$viewHelper281->setRenderChildrenClosure($renderChildrenClosure280);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output249 .= $viewHelper281->initializeArgumentsAndRender();

$output249 .= '
			</p>
			<ul class="f3-widget-paginator">
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments282 = array();
// Rendering Boolean node
$arguments282['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext));
$arguments282['then'] = NULL;
$arguments282['else'] = NULL;
$renderChildrenClosure283 = function() use ($renderingContext, $self) {
$output284 = '';

$output284 .= '
					<li class="previous">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments285 = array();
// Rendering Boolean node
$arguments285['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext), 1);
$arguments285['then'] = NULL;
$arguments285['else'] = NULL;
$renderChildrenClosure286 = function() use ($renderingContext, $self) {
$output287 = '';

$output287 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments288 = array();
$renderChildrenClosure289 = function() use ($renderingContext, $self) {
$output290 = '';

$output290 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments291 = array();
// Rendering Array
$array292 = array();
$array292['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments291['arguments'] = $array292;
$arguments291['additionalAttributes'] = NULL;
$arguments291['action'] = NULL;
$arguments291['section'] = '';
$arguments291['format'] = '';
$arguments291['ajax'] = false;
$arguments291['class'] = NULL;
$arguments291['dir'] = NULL;
$arguments291['id'] = NULL;
$arguments291['lang'] = NULL;
$arguments291['style'] = NULL;
$arguments291['title'] = NULL;
$arguments291['accesskey'] = NULL;
$arguments291['tabindex'] = NULL;
$arguments291['onclick'] = NULL;
$arguments291['name'] = NULL;
$arguments291['rel'] = NULL;
$arguments291['rev'] = NULL;
$arguments291['target'] = NULL;
$renderChildrenClosure293 = function() use ($renderingContext, $self) {
$output294 = '';

$output294 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments295 = array();
$arguments295['key'] = 'paginate_previous';
$arguments295['id'] = NULL;
$arguments295['default'] = NULL;
$arguments295['htmlEscape'] = NULL;
$arguments295['arguments'] = NULL;
$arguments295['extensionName'] = NULL;
$renderChildrenClosure296 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper297 = $self->getViewHelper('$viewHelper297', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper297->setArguments($arguments295);
$viewHelper297->setRenderingContext($renderingContext);
$viewHelper297->setRenderChildrenClosure($renderChildrenClosure296);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output294 .= $viewHelper297->initializeArgumentsAndRender();

$output294 .= '
								';
return $output294;
};
$viewHelper298 = $self->getViewHelper('$viewHelper298', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper298->setArguments($arguments291);
$viewHelper298->setRenderingContext($renderingContext);
$viewHelper298->setRenderChildrenClosure($renderChildrenClosure293);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output290 .= $viewHelper298->initializeArgumentsAndRender();

$output290 .= '
							';
return $output290;
};
$viewHelper299 = $self->getViewHelper('$viewHelper299', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper299->setArguments($arguments288);
$viewHelper299->setRenderingContext($renderingContext);
$viewHelper299->setRenderChildrenClosure($renderChildrenClosure289);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output287 .= $viewHelper299->initializeArgumentsAndRender();

$output287 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments300 = array();
$renderChildrenClosure301 = function() use ($renderingContext, $self) {
$output302 = '';

$output302 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments303 = array();
$arguments303['additionalAttributes'] = NULL;
$arguments303['action'] = NULL;
$arguments303['arguments'] = array (
);
$arguments303['section'] = '';
$arguments303['format'] = '';
$arguments303['ajax'] = false;
$arguments303['class'] = NULL;
$arguments303['dir'] = NULL;
$arguments303['id'] = NULL;
$arguments303['lang'] = NULL;
$arguments303['style'] = NULL;
$arguments303['title'] = NULL;
$arguments303['accesskey'] = NULL;
$arguments303['tabindex'] = NULL;
$arguments303['onclick'] = NULL;
$arguments303['name'] = NULL;
$arguments303['rel'] = NULL;
$arguments303['rev'] = NULL;
$arguments303['target'] = NULL;
$renderChildrenClosure304 = function() use ($renderingContext, $self) {
$output305 = '';

$output305 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments306 = array();
$arguments306['key'] = 'paginate_previous';
$arguments306['id'] = NULL;
$arguments306['default'] = NULL;
$arguments306['htmlEscape'] = NULL;
$arguments306['arguments'] = NULL;
$arguments306['extensionName'] = NULL;
$renderChildrenClosure307 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper308 = $self->getViewHelper('$viewHelper308', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper308->setArguments($arguments306);
$viewHelper308->setRenderingContext($renderingContext);
$viewHelper308->setRenderChildrenClosure($renderChildrenClosure307);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output305 .= $viewHelper308->initializeArgumentsAndRender();

$output305 .= '
								';
return $output305;
};
$viewHelper309 = $self->getViewHelper('$viewHelper309', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper309->setArguments($arguments303);
$viewHelper309->setRenderingContext($renderingContext);
$viewHelper309->setRenderChildrenClosure($renderChildrenClosure304);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output302 .= $viewHelper309->initializeArgumentsAndRender();

$output302 .= '
							';
return $output302;
};
$viewHelper310 = $self->getViewHelper('$viewHelper310', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper310->setArguments($arguments300);
$viewHelper310->setRenderingContext($renderingContext);
$viewHelper310->setRenderChildrenClosure($renderChildrenClosure301);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output287 .= $viewHelper310->initializeArgumentsAndRender();

$output287 .= '
						';
return $output287;
};
$arguments285['__thenClosure'] = function() use ($renderingContext, $self) {
$output311 = '';

$output311 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments312 = array();
// Rendering Array
$array313 = array();
$array313['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.previousPage', $renderingContext);
$arguments312['arguments'] = $array313;
$arguments312['additionalAttributes'] = NULL;
$arguments312['action'] = NULL;
$arguments312['section'] = '';
$arguments312['format'] = '';
$arguments312['ajax'] = false;
$arguments312['class'] = NULL;
$arguments312['dir'] = NULL;
$arguments312['id'] = NULL;
$arguments312['lang'] = NULL;
$arguments312['style'] = NULL;
$arguments312['title'] = NULL;
$arguments312['accesskey'] = NULL;
$arguments312['tabindex'] = NULL;
$arguments312['onclick'] = NULL;
$arguments312['name'] = NULL;
$arguments312['rel'] = NULL;
$arguments312['rev'] = NULL;
$arguments312['target'] = NULL;
$renderChildrenClosure314 = function() use ($renderingContext, $self) {
$output315 = '';

$output315 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments316 = array();
$arguments316['key'] = 'paginate_previous';
$arguments316['id'] = NULL;
$arguments316['default'] = NULL;
$arguments316['htmlEscape'] = NULL;
$arguments316['arguments'] = NULL;
$arguments316['extensionName'] = NULL;
$renderChildrenClosure317 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper318 = $self->getViewHelper('$viewHelper318', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper318->setArguments($arguments316);
$viewHelper318->setRenderingContext($renderingContext);
$viewHelper318->setRenderChildrenClosure($renderChildrenClosure317);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output315 .= $viewHelper318->initializeArgumentsAndRender();

$output315 .= '
								';
return $output315;
};
$viewHelper319 = $self->getViewHelper('$viewHelper319', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper319->setArguments($arguments312);
$viewHelper319->setRenderingContext($renderingContext);
$viewHelper319->setRenderChildrenClosure($renderChildrenClosure314);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output311 .= $viewHelper319->initializeArgumentsAndRender();

$output311 .= '
							';
return $output311;
};
$arguments285['__elseClosure'] = function() use ($renderingContext, $self) {
$output320 = '';

$output320 .= '
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments321 = array();
$arguments321['additionalAttributes'] = NULL;
$arguments321['action'] = NULL;
$arguments321['arguments'] = array (
);
$arguments321['section'] = '';
$arguments321['format'] = '';
$arguments321['ajax'] = false;
$arguments321['class'] = NULL;
$arguments321['dir'] = NULL;
$arguments321['id'] = NULL;
$arguments321['lang'] = NULL;
$arguments321['style'] = NULL;
$arguments321['title'] = NULL;
$arguments321['accesskey'] = NULL;
$arguments321['tabindex'] = NULL;
$arguments321['onclick'] = NULL;
$arguments321['name'] = NULL;
$arguments321['rel'] = NULL;
$arguments321['rev'] = NULL;
$arguments321['target'] = NULL;
$renderChildrenClosure322 = function() use ($renderingContext, $self) {
$output323 = '';

$output323 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments324 = array();
$arguments324['key'] = 'paginate_previous';
$arguments324['id'] = NULL;
$arguments324['default'] = NULL;
$arguments324['htmlEscape'] = NULL;
$arguments324['arguments'] = NULL;
$arguments324['extensionName'] = NULL;
$renderChildrenClosure325 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper326 = $self->getViewHelper('$viewHelper326', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper326->setArguments($arguments324);
$viewHelper326->setRenderingContext($renderingContext);
$viewHelper326->setRenderChildrenClosure($renderChildrenClosure325);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output323 .= $viewHelper326->initializeArgumentsAndRender();

$output323 .= '
								';
return $output323;
};
$viewHelper327 = $self->getViewHelper('$viewHelper327', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper327->setArguments($arguments321);
$viewHelper327->setRenderingContext($renderingContext);
$viewHelper327->setRenderChildrenClosure($renderChildrenClosure322);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output320 .= $viewHelper327->initializeArgumentsAndRender();

$output320 .= '
							';
return $output320;
};
$viewHelper328 = $self->getViewHelper('$viewHelper328', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper328->setArguments($arguments285);
$viewHelper328->setRenderingContext($renderingContext);
$viewHelper328->setRenderChildrenClosure($renderChildrenClosure286);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output284 .= $viewHelper328->initializeArgumentsAndRender();

$output284 .= '
					</li>
				';
return $output284;
};
$viewHelper329 = $self->getViewHelper('$viewHelper329', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper329->setArguments($arguments282);
$viewHelper329->setRenderingContext($renderingContext);
$viewHelper329->setRenderChildrenClosure($renderChildrenClosure283);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper329->initializeArgumentsAndRender();

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments330 = array();
// Rendering Boolean node
$arguments330['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.displayRangeStart', $renderingContext), 1);
$arguments330['then'] = NULL;
$arguments330['else'] = NULL;
$renderChildrenClosure331 = function() use ($renderingContext, $self) {
$output332 = '';

$output332 .= '
					<li class="first">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments333 = array();
$arguments333['additionalAttributes'] = NULL;
$arguments333['action'] = NULL;
$arguments333['arguments'] = array (
);
$arguments333['section'] = '';
$arguments333['format'] = '';
$arguments333['ajax'] = false;
$arguments333['class'] = NULL;
$arguments333['dir'] = NULL;
$arguments333['id'] = NULL;
$arguments333['lang'] = NULL;
$arguments333['style'] = NULL;
$arguments333['title'] = NULL;
$arguments333['accesskey'] = NULL;
$arguments333['tabindex'] = NULL;
$arguments333['onclick'] = NULL;
$arguments333['name'] = NULL;
$arguments333['rel'] = NULL;
$arguments333['rev'] = NULL;
$arguments333['target'] = NULL;
$renderChildrenClosure334 = function() use ($renderingContext, $self) {
return '1';
};
$viewHelper335 = $self->getViewHelper('$viewHelper335', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper335->setArguments($arguments333);
$viewHelper335->setRenderingContext($renderingContext);
$viewHelper335->setRenderChildrenClosure($renderChildrenClosure334);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output332 .= $viewHelper335->initializeArgumentsAndRender();

$output332 .= '
					</li>
				';
return $output332;
};
$viewHelper336 = $self->getViewHelper('$viewHelper336', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper336->setArguments($arguments330);
$viewHelper336->setRenderingContext($renderingContext);
$viewHelper336->setRenderChildrenClosure($renderChildrenClosure331);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper336->initializeArgumentsAndRender();

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments337 = array();
// Rendering Boolean node
$arguments337['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.hasLessPages', $renderingContext));
$arguments337['then'] = NULL;
$arguments337['else'] = NULL;
$renderChildrenClosure338 = function() use ($renderingContext, $self) {
return '
					<li>....</li>
				';
};
$viewHelper339 = $self->getViewHelper('$viewHelper339', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper339->setArguments($arguments337);
$viewHelper339->setRenderingContext($renderingContext);
$viewHelper339->setRenderChildrenClosure($renderChildrenClosure338);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper339->initializeArgumentsAndRender();

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper
$arguments340 = array();
$arguments340['each'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.pages', $renderingContext);
$arguments340['as'] = 'page';
$arguments340['key'] = '';
$arguments340['reverse'] = false;
$arguments340['iteration'] = NULL;
$renderChildrenClosure341 = function() use ($renderingContext, $self) {
$output342 = '';

$output342 .= '
					';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments343 = array();
// Rendering Boolean node
$arguments343['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.isCurrent', $renderingContext));
$arguments343['then'] = NULL;
$arguments343['else'] = NULL;
$renderChildrenClosure344 = function() use ($renderingContext, $self) {
$output345 = '';

$output345 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments346 = array();
$renderChildrenClosure347 = function() use ($renderingContext, $self) {
$output348 = '';

$output348 .= '
							<li class="current">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments349 = array();
$arguments349['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments349['keepQuotes'] = false;
$arguments349['encoding'] = NULL;
$arguments349['doubleEncode'] = true;
$renderChildrenClosure350 = function() use ($renderingContext, $self) {
return NULL;
};
$value351 = ($arguments349['value'] !== NULL ? $arguments349['value'] : $renderChildrenClosure350());

$output348 .= (!is_string($value351) ? $value351 : htmlspecialchars($value351, ($arguments349['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments349['encoding'] !== NULL ? $arguments349['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments349['doubleEncode']));

$output348 .= '</li>
						';
return $output348;
};
$viewHelper352 = $self->getViewHelper('$viewHelper352', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper352->setArguments($arguments346);
$viewHelper352->setRenderingContext($renderingContext);
$viewHelper352->setRenderChildrenClosure($renderChildrenClosure347);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output345 .= $viewHelper352->initializeArgumentsAndRender();

$output345 .= '
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments353 = array();
$renderChildrenClosure354 = function() use ($renderingContext, $self) {
$output355 = '';

$output355 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments356 = array();
// Rendering Boolean node
$arguments356['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext), 1);
$arguments356['then'] = NULL;
$arguments356['else'] = NULL;
$renderChildrenClosure357 = function() use ($renderingContext, $self) {
$output358 = '';

$output358 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments359 = array();
$renderChildrenClosure360 = function() use ($renderingContext, $self) {
$output361 = '';

$output361 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments362 = array();
// Rendering Array
$array363 = array();
$array363['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments362['arguments'] = $array363;
$arguments362['additionalAttributes'] = NULL;
$arguments362['action'] = NULL;
$arguments362['section'] = '';
$arguments362['format'] = '';
$arguments362['ajax'] = false;
$arguments362['class'] = NULL;
$arguments362['dir'] = NULL;
$arguments362['id'] = NULL;
$arguments362['lang'] = NULL;
$arguments362['style'] = NULL;
$arguments362['title'] = NULL;
$arguments362['accesskey'] = NULL;
$arguments362['tabindex'] = NULL;
$arguments362['onclick'] = NULL;
$arguments362['name'] = NULL;
$arguments362['rel'] = NULL;
$arguments362['rev'] = NULL;
$arguments362['target'] = NULL;
$renderChildrenClosure364 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments365 = array();
$arguments365['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments365['keepQuotes'] = false;
$arguments365['encoding'] = NULL;
$arguments365['doubleEncode'] = true;
$renderChildrenClosure366 = function() use ($renderingContext, $self) {
return NULL;
};
$value367 = ($arguments365['value'] !== NULL ? $arguments365['value'] : $renderChildrenClosure366());
return (!is_string($value367) ? $value367 : htmlspecialchars($value367, ($arguments365['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments365['encoding'] !== NULL ? $arguments365['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments365['doubleEncode']));
};
$viewHelper368 = $self->getViewHelper('$viewHelper368', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper368->setArguments($arguments362);
$viewHelper368->setRenderingContext($renderingContext);
$viewHelper368->setRenderChildrenClosure($renderChildrenClosure364);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output361 .= $viewHelper368->initializeArgumentsAndRender();

$output361 .= '
									';
return $output361;
};
$viewHelper369 = $self->getViewHelper('$viewHelper369', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper369->setArguments($arguments359);
$viewHelper369->setRenderingContext($renderingContext);
$viewHelper369->setRenderChildrenClosure($renderChildrenClosure360);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output358 .= $viewHelper369->initializeArgumentsAndRender();

$output358 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments370 = array();
$renderChildrenClosure371 = function() use ($renderingContext, $self) {
$output372 = '';

$output372 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments373 = array();
$arguments373['additionalAttributes'] = NULL;
$arguments373['action'] = NULL;
$arguments373['arguments'] = array (
);
$arguments373['section'] = '';
$arguments373['format'] = '';
$arguments373['ajax'] = false;
$arguments373['class'] = NULL;
$arguments373['dir'] = NULL;
$arguments373['id'] = NULL;
$arguments373['lang'] = NULL;
$arguments373['style'] = NULL;
$arguments373['title'] = NULL;
$arguments373['accesskey'] = NULL;
$arguments373['tabindex'] = NULL;
$arguments373['onclick'] = NULL;
$arguments373['name'] = NULL;
$arguments373['rel'] = NULL;
$arguments373['rev'] = NULL;
$arguments373['target'] = NULL;
$renderChildrenClosure374 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments375 = array();
$arguments375['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments375['keepQuotes'] = false;
$arguments375['encoding'] = NULL;
$arguments375['doubleEncode'] = true;
$renderChildrenClosure376 = function() use ($renderingContext, $self) {
return NULL;
};
$value377 = ($arguments375['value'] !== NULL ? $arguments375['value'] : $renderChildrenClosure376());
return (!is_string($value377) ? $value377 : htmlspecialchars($value377, ($arguments375['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments375['encoding'] !== NULL ? $arguments375['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments375['doubleEncode']));
};
$viewHelper378 = $self->getViewHelper('$viewHelper378', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper378->setArguments($arguments373);
$viewHelper378->setRenderingContext($renderingContext);
$viewHelper378->setRenderChildrenClosure($renderChildrenClosure374);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output372 .= $viewHelper378->initializeArgumentsAndRender();

$output372 .= '
									';
return $output372;
};
$viewHelper379 = $self->getViewHelper('$viewHelper379', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper379->setArguments($arguments370);
$viewHelper379->setRenderingContext($renderingContext);
$viewHelper379->setRenderChildrenClosure($renderChildrenClosure371);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output358 .= $viewHelper379->initializeArgumentsAndRender();

$output358 .= '
								';
return $output358;
};
$arguments356['__thenClosure'] = function() use ($renderingContext, $self) {
$output380 = '';

$output380 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments381 = array();
// Rendering Array
$array382 = array();
$array382['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments381['arguments'] = $array382;
$arguments381['additionalAttributes'] = NULL;
$arguments381['action'] = NULL;
$arguments381['section'] = '';
$arguments381['format'] = '';
$arguments381['ajax'] = false;
$arguments381['class'] = NULL;
$arguments381['dir'] = NULL;
$arguments381['id'] = NULL;
$arguments381['lang'] = NULL;
$arguments381['style'] = NULL;
$arguments381['title'] = NULL;
$arguments381['accesskey'] = NULL;
$arguments381['tabindex'] = NULL;
$arguments381['onclick'] = NULL;
$arguments381['name'] = NULL;
$arguments381['rel'] = NULL;
$arguments381['rev'] = NULL;
$arguments381['target'] = NULL;
$renderChildrenClosure383 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments384 = array();
$arguments384['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments384['keepQuotes'] = false;
$arguments384['encoding'] = NULL;
$arguments384['doubleEncode'] = true;
$renderChildrenClosure385 = function() use ($renderingContext, $self) {
return NULL;
};
$value386 = ($arguments384['value'] !== NULL ? $arguments384['value'] : $renderChildrenClosure385());
return (!is_string($value386) ? $value386 : htmlspecialchars($value386, ($arguments384['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments384['encoding'] !== NULL ? $arguments384['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments384['doubleEncode']));
};
$viewHelper387 = $self->getViewHelper('$viewHelper387', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper387->setArguments($arguments381);
$viewHelper387->setRenderingContext($renderingContext);
$viewHelper387->setRenderChildrenClosure($renderChildrenClosure383);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output380 .= $viewHelper387->initializeArgumentsAndRender();

$output380 .= '
									';
return $output380;
};
$arguments356['__elseClosure'] = function() use ($renderingContext, $self) {
$output388 = '';

$output388 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments389 = array();
$arguments389['additionalAttributes'] = NULL;
$arguments389['action'] = NULL;
$arguments389['arguments'] = array (
);
$arguments389['section'] = '';
$arguments389['format'] = '';
$arguments389['ajax'] = false;
$arguments389['class'] = NULL;
$arguments389['dir'] = NULL;
$arguments389['id'] = NULL;
$arguments389['lang'] = NULL;
$arguments389['style'] = NULL;
$arguments389['title'] = NULL;
$arguments389['accesskey'] = NULL;
$arguments389['tabindex'] = NULL;
$arguments389['onclick'] = NULL;
$arguments389['name'] = NULL;
$arguments389['rel'] = NULL;
$arguments389['rev'] = NULL;
$arguments389['target'] = NULL;
$renderChildrenClosure390 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments391 = array();
$arguments391['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments391['keepQuotes'] = false;
$arguments391['encoding'] = NULL;
$arguments391['doubleEncode'] = true;
$renderChildrenClosure392 = function() use ($renderingContext, $self) {
return NULL;
};
$value393 = ($arguments391['value'] !== NULL ? $arguments391['value'] : $renderChildrenClosure392());
return (!is_string($value393) ? $value393 : htmlspecialchars($value393, ($arguments391['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments391['encoding'] !== NULL ? $arguments391['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments391['doubleEncode']));
};
$viewHelper394 = $self->getViewHelper('$viewHelper394', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper394->setArguments($arguments389);
$viewHelper394->setRenderingContext($renderingContext);
$viewHelper394->setRenderChildrenClosure($renderChildrenClosure390);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output388 .= $viewHelper394->initializeArgumentsAndRender();

$output388 .= '
									';
return $output388;
};
$viewHelper395 = $self->getViewHelper('$viewHelper395', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper395->setArguments($arguments356);
$viewHelper395->setRenderingContext($renderingContext);
$viewHelper395->setRenderChildrenClosure($renderChildrenClosure357);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output355 .= $viewHelper395->initializeArgumentsAndRender();

$output355 .= '
							</li>
						';
return $output355;
};
$viewHelper396 = $self->getViewHelper('$viewHelper396', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper396->setArguments($arguments353);
$viewHelper396->setRenderingContext($renderingContext);
$viewHelper396->setRenderChildrenClosure($renderChildrenClosure354);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output345 .= $viewHelper396->initializeArgumentsAndRender();

$output345 .= '
					';
return $output345;
};
$arguments343['__thenClosure'] = function() use ($renderingContext, $self) {
$output397 = '';

$output397 .= '
							<li class="current">';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments398 = array();
$arguments398['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments398['keepQuotes'] = false;
$arguments398['encoding'] = NULL;
$arguments398['doubleEncode'] = true;
$renderChildrenClosure399 = function() use ($renderingContext, $self) {
return NULL;
};
$value400 = ($arguments398['value'] !== NULL ? $arguments398['value'] : $renderChildrenClosure399());

$output397 .= (!is_string($value400) ? $value400 : htmlspecialchars($value400, ($arguments398['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments398['encoding'] !== NULL ? $arguments398['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments398['doubleEncode']));

$output397 .= '</li>
						';
return $output397;
};
$arguments343['__elseClosure'] = function() use ($renderingContext, $self) {
$output401 = '';

$output401 .= '
							<li>
								';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments402 = array();
// Rendering Boolean node
$arguments402['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('>', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext), 1);
$arguments402['then'] = NULL;
$arguments402['else'] = NULL;
$renderChildrenClosure403 = function() use ($renderingContext, $self) {
$output404 = '';

$output404 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper
$arguments405 = array();
$renderChildrenClosure406 = function() use ($renderingContext, $self) {
$output407 = '';

$output407 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments408 = array();
// Rendering Array
$array409 = array();
$array409['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments408['arguments'] = $array409;
$arguments408['additionalAttributes'] = NULL;
$arguments408['action'] = NULL;
$arguments408['section'] = '';
$arguments408['format'] = '';
$arguments408['ajax'] = false;
$arguments408['class'] = NULL;
$arguments408['dir'] = NULL;
$arguments408['id'] = NULL;
$arguments408['lang'] = NULL;
$arguments408['style'] = NULL;
$arguments408['title'] = NULL;
$arguments408['accesskey'] = NULL;
$arguments408['tabindex'] = NULL;
$arguments408['onclick'] = NULL;
$arguments408['name'] = NULL;
$arguments408['rel'] = NULL;
$arguments408['rev'] = NULL;
$arguments408['target'] = NULL;
$renderChildrenClosure410 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments411 = array();
$arguments411['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments411['keepQuotes'] = false;
$arguments411['encoding'] = NULL;
$arguments411['doubleEncode'] = true;
$renderChildrenClosure412 = function() use ($renderingContext, $self) {
return NULL;
};
$value413 = ($arguments411['value'] !== NULL ? $arguments411['value'] : $renderChildrenClosure412());
return (!is_string($value413) ? $value413 : htmlspecialchars($value413, ($arguments411['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments411['encoding'] !== NULL ? $arguments411['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments411['doubleEncode']));
};
$viewHelper414 = $self->getViewHelper('$viewHelper414', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper414->setArguments($arguments408);
$viewHelper414->setRenderingContext($renderingContext);
$viewHelper414->setRenderChildrenClosure($renderChildrenClosure410);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output407 .= $viewHelper414->initializeArgumentsAndRender();

$output407 .= '
									';
return $output407;
};
$viewHelper415 = $self->getViewHelper('$viewHelper415', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper');
$viewHelper415->setArguments($arguments405);
$viewHelper415->setRenderingContext($renderingContext);
$viewHelper415->setRenderChildrenClosure($renderChildrenClosure406);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ThenViewHelper

$output404 .= $viewHelper415->initializeArgumentsAndRender();

$output404 .= '
									';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper
$arguments416 = array();
$renderChildrenClosure417 = function() use ($renderingContext, $self) {
$output418 = '';

$output418 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments419 = array();
$arguments419['additionalAttributes'] = NULL;
$arguments419['action'] = NULL;
$arguments419['arguments'] = array (
);
$arguments419['section'] = '';
$arguments419['format'] = '';
$arguments419['ajax'] = false;
$arguments419['class'] = NULL;
$arguments419['dir'] = NULL;
$arguments419['id'] = NULL;
$arguments419['lang'] = NULL;
$arguments419['style'] = NULL;
$arguments419['title'] = NULL;
$arguments419['accesskey'] = NULL;
$arguments419['tabindex'] = NULL;
$arguments419['onclick'] = NULL;
$arguments419['name'] = NULL;
$arguments419['rel'] = NULL;
$arguments419['rev'] = NULL;
$arguments419['target'] = NULL;
$renderChildrenClosure420 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments421 = array();
$arguments421['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments421['keepQuotes'] = false;
$arguments421['encoding'] = NULL;
$arguments421['doubleEncode'] = true;
$renderChildrenClosure422 = function() use ($renderingContext, $self) {
return NULL;
};
$value423 = ($arguments421['value'] !== NULL ? $arguments421['value'] : $renderChildrenClosure422());
return (!is_string($value423) ? $value423 : htmlspecialchars($value423, ($arguments421['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments421['encoding'] !== NULL ? $arguments421['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments421['doubleEncode']));
};
$viewHelper424 = $self->getViewHelper('$viewHelper424', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper424->setArguments($arguments419);
$viewHelper424->setRenderingContext($renderingContext);
$viewHelper424->setRenderChildrenClosure($renderChildrenClosure420);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output418 .= $viewHelper424->initializeArgumentsAndRender();

$output418 .= '
									';
return $output418;
};
$viewHelper425 = $self->getViewHelper('$viewHelper425', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper');
$viewHelper425->setArguments($arguments416);
$viewHelper425->setRenderingContext($renderingContext);
$viewHelper425->setRenderChildrenClosure($renderChildrenClosure417);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\ElseViewHelper

$output404 .= $viewHelper425->initializeArgumentsAndRender();

$output404 .= '
								';
return $output404;
};
$arguments402['__thenClosure'] = function() use ($renderingContext, $self) {
$output426 = '';

$output426 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments427 = array();
// Rendering Array
$array428 = array();
$array428['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments427['arguments'] = $array428;
$arguments427['additionalAttributes'] = NULL;
$arguments427['action'] = NULL;
$arguments427['section'] = '';
$arguments427['format'] = '';
$arguments427['ajax'] = false;
$arguments427['class'] = NULL;
$arguments427['dir'] = NULL;
$arguments427['id'] = NULL;
$arguments427['lang'] = NULL;
$arguments427['style'] = NULL;
$arguments427['title'] = NULL;
$arguments427['accesskey'] = NULL;
$arguments427['tabindex'] = NULL;
$arguments427['onclick'] = NULL;
$arguments427['name'] = NULL;
$arguments427['rel'] = NULL;
$arguments427['rev'] = NULL;
$arguments427['target'] = NULL;
$renderChildrenClosure429 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments430 = array();
$arguments430['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments430['keepQuotes'] = false;
$arguments430['encoding'] = NULL;
$arguments430['doubleEncode'] = true;
$renderChildrenClosure431 = function() use ($renderingContext, $self) {
return NULL;
};
$value432 = ($arguments430['value'] !== NULL ? $arguments430['value'] : $renderChildrenClosure431());
return (!is_string($value432) ? $value432 : htmlspecialchars($value432, ($arguments430['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments430['encoding'] !== NULL ? $arguments430['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments430['doubleEncode']));
};
$viewHelper433 = $self->getViewHelper('$viewHelper433', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper433->setArguments($arguments427);
$viewHelper433->setRenderingContext($renderingContext);
$viewHelper433->setRenderChildrenClosure($renderChildrenClosure429);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output426 .= $viewHelper433->initializeArgumentsAndRender();

$output426 .= '
									';
return $output426;
};
$arguments402['__elseClosure'] = function() use ($renderingContext, $self) {
$output434 = '';

$output434 .= '
										';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments435 = array();
$arguments435['additionalAttributes'] = NULL;
$arguments435['action'] = NULL;
$arguments435['arguments'] = array (
);
$arguments435['section'] = '';
$arguments435['format'] = '';
$arguments435['ajax'] = false;
$arguments435['class'] = NULL;
$arguments435['dir'] = NULL;
$arguments435['id'] = NULL;
$arguments435['lang'] = NULL;
$arguments435['style'] = NULL;
$arguments435['title'] = NULL;
$arguments435['accesskey'] = NULL;
$arguments435['tabindex'] = NULL;
$arguments435['onclick'] = NULL;
$arguments435['name'] = NULL;
$arguments435['rel'] = NULL;
$arguments435['rev'] = NULL;
$arguments435['target'] = NULL;
$renderChildrenClosure436 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments437 = array();
$arguments437['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'page.number', $renderingContext);
$arguments437['keepQuotes'] = false;
$arguments437['encoding'] = NULL;
$arguments437['doubleEncode'] = true;
$renderChildrenClosure438 = function() use ($renderingContext, $self) {
return NULL;
};
$value439 = ($arguments437['value'] !== NULL ? $arguments437['value'] : $renderChildrenClosure438());
return (!is_string($value439) ? $value439 : htmlspecialchars($value439, ($arguments437['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments437['encoding'] !== NULL ? $arguments437['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments437['doubleEncode']));
};
$viewHelper440 = $self->getViewHelper('$viewHelper440', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper440->setArguments($arguments435);
$viewHelper440->setRenderingContext($renderingContext);
$viewHelper440->setRenderChildrenClosure($renderChildrenClosure436);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output434 .= $viewHelper440->initializeArgumentsAndRender();

$output434 .= '
									';
return $output434;
};
$viewHelper441 = $self->getViewHelper('$viewHelper441', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper441->setArguments($arguments402);
$viewHelper441->setRenderingContext($renderingContext);
$viewHelper441->setRenderChildrenClosure($renderChildrenClosure403);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output401 .= $viewHelper441->initializeArgumentsAndRender();

$output401 .= '
							</li>
						';
return $output401;
};
$viewHelper442 = $self->getViewHelper('$viewHelper442', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper442->setArguments($arguments343);
$viewHelper442->setRenderingContext($renderingContext);
$viewHelper442->setRenderChildrenClosure($renderChildrenClosure344);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output342 .= $viewHelper442->initializeArgumentsAndRender();

$output342 .= '
				';
return $output342;
};

$output249 .= TYPO3\CMS\Fluid\ViewHelpers\ForViewHelper::renderStatic($arguments340, $renderChildrenClosure341, $renderingContext);

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments443 = array();
// Rendering Boolean node
$arguments443['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.hasMorePages', $renderingContext));
$arguments443['then'] = NULL;
$arguments443['else'] = NULL;
$renderChildrenClosure444 = function() use ($renderingContext, $self) {
return '
					<li>....</li>
				';
};
$viewHelper445 = $self->getViewHelper('$viewHelper445', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper445->setArguments($arguments443);
$viewHelper445->setRenderingContext($renderingContext);
$viewHelper445->setRenderChildrenClosure($renderChildrenClosure444);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper445->initializeArgumentsAndRender();

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments446 = array();
// Rendering Boolean node
$arguments446['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::evaluateComparator('<', TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.displayRangeEnd', $renderingContext), TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext));
$arguments446['then'] = NULL;
$arguments446['else'] = NULL;
$renderChildrenClosure447 = function() use ($renderingContext, $self) {
$output448 = '';

$output448 .= '
					<li class="last">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments449 = array();
// Rendering Array
$array450 = array();
$array450['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments449['arguments'] = $array450;
$arguments449['additionalAttributes'] = NULL;
$arguments449['action'] = NULL;
$arguments449['section'] = '';
$arguments449['format'] = '';
$arguments449['ajax'] = false;
$arguments449['class'] = NULL;
$arguments449['dir'] = NULL;
$arguments449['id'] = NULL;
$arguments449['lang'] = NULL;
$arguments449['style'] = NULL;
$arguments449['title'] = NULL;
$arguments449['accesskey'] = NULL;
$arguments449['tabindex'] = NULL;
$arguments449['onclick'] = NULL;
$arguments449['name'] = NULL;
$arguments449['rel'] = NULL;
$arguments449['rev'] = NULL;
$arguments449['target'] = NULL;
$renderChildrenClosure451 = function() use ($renderingContext, $self) {
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Format\HtmlspecialcharsViewHelper
$arguments452 = array();
$arguments452['value'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.numberOfPages', $renderingContext);
$arguments452['keepQuotes'] = false;
$arguments452['encoding'] = NULL;
$arguments452['doubleEncode'] = true;
$renderChildrenClosure453 = function() use ($renderingContext, $self) {
return NULL;
};
$value454 = ($arguments452['value'] !== NULL ? $arguments452['value'] : $renderChildrenClosure453());
return (!is_string($value454) ? $value454 : htmlspecialchars($value454, ($arguments452['keepQuotes'] ? ENT_NOQUOTES : ENT_COMPAT), ($arguments452['encoding'] !== NULL ? $arguments452['encoding'] : \TYPO3\CMS\Fluid\Core\Compiler\AbstractCompiledTemplate::resolveDefaultEncoding()), $arguments452['doubleEncode']));
};
$viewHelper455 = $self->getViewHelper('$viewHelper455', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper455->setArguments($arguments449);
$viewHelper455->setRenderingContext($renderingContext);
$viewHelper455->setRenderChildrenClosure($renderChildrenClosure451);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output448 .= $viewHelper455->initializeArgumentsAndRender();

$output448 .= '
					</li>
				';
return $output448;
};
$viewHelper456 = $self->getViewHelper('$viewHelper456', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper456->setArguments($arguments446);
$viewHelper456->setRenderingContext($renderingContext);
$viewHelper456->setRenderChildrenClosure($renderChildrenClosure447);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper456->initializeArgumentsAndRender();

$output249 .= '
				';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper
$arguments457 = array();
// Rendering Boolean node
$arguments457['condition'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\BooleanNode::convertToBoolean(TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext));
$arguments457['then'] = NULL;
$arguments457['else'] = NULL;
$renderChildrenClosure458 = function() use ($renderingContext, $self) {
$output459 = '';

$output459 .= '
					<li class="last next">
						';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper
$arguments460 = array();
// Rendering Array
$array461 = array();
$array461['currentPage'] = TYPO3\CMS\Fluid\Core\Parser\SyntaxTree\ObjectAccessorNode::getPropertyPath($renderingContext->getTemplateVariableContainer(), 'pagination.nextPage', $renderingContext);
$arguments460['arguments'] = $array461;
$arguments460['additionalAttributes'] = NULL;
$arguments460['action'] = NULL;
$arguments460['section'] = '';
$arguments460['format'] = '';
$arguments460['ajax'] = false;
$arguments460['class'] = NULL;
$arguments460['dir'] = NULL;
$arguments460['id'] = NULL;
$arguments460['lang'] = NULL;
$arguments460['style'] = NULL;
$arguments460['title'] = NULL;
$arguments460['accesskey'] = NULL;
$arguments460['tabindex'] = NULL;
$arguments460['onclick'] = NULL;
$arguments460['name'] = NULL;
$arguments460['rel'] = NULL;
$arguments460['rev'] = NULL;
$arguments460['target'] = NULL;
$renderChildrenClosure462 = function() use ($renderingContext, $self) {
$output463 = '';

$output463 .= '
							';
// Rendering ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper
$arguments464 = array();
$arguments464['key'] = 'paginate_next';
$arguments464['id'] = NULL;
$arguments464['default'] = NULL;
$arguments464['htmlEscape'] = NULL;
$arguments464['arguments'] = NULL;
$arguments464['extensionName'] = NULL;
$renderChildrenClosure465 = function() use ($renderingContext, $self) {
return NULL;
};
$viewHelper466 = $self->getViewHelper('$viewHelper466', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper');
$viewHelper466->setArguments($arguments464);
$viewHelper466->setRenderingContext($renderingContext);
$viewHelper466->setRenderChildrenClosure($renderChildrenClosure465);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\TranslateViewHelper

$output463 .= $viewHelper466->initializeArgumentsAndRender();

$output463 .= '
						';
return $output463;
};
$viewHelper467 = $self->getViewHelper('$viewHelper467', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper');
$viewHelper467->setArguments($arguments460);
$viewHelper467->setRenderingContext($renderingContext);
$viewHelper467->setRenderChildrenClosure($renderChildrenClosure462);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\Widget\LinkViewHelper

$output459 .= $viewHelper467->initializeArgumentsAndRender();

$output459 .= '
					</li>
				';
return $output459;
};
$viewHelper468 = $self->getViewHelper('$viewHelper468', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper468->setArguments($arguments457);
$viewHelper468->setRenderingContext($renderingContext);
$viewHelper468->setRenderChildrenClosure($renderChildrenClosure458);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output249 .= $viewHelper468->initializeArgumentsAndRender();

$output249 .= '
			</ul>
		</div>
		<div class="news-clear"></div>
	';
return $output249;
};
$viewHelper469 = $self->getViewHelper('$viewHelper469', $renderingContext, 'TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper');
$viewHelper469->setArguments($arguments247);
$viewHelper469->setRenderingContext($renderingContext);
$viewHelper469->setRenderChildrenClosure($renderChildrenClosure248);
// End of ViewHelper TYPO3\CMS\Fluid\ViewHelpers\IfViewHelper

$output246 .= $viewHelper469->initializeArgumentsAndRender();

$output246 .= '
';
return $output246;
};

$output224 .= '';

$output224 .= '
';

return $output224;
}


}
#1399936121    130380    